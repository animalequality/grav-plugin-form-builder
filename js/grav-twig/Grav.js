/**
 * Copyright (C) 2019, 2020 Raphaël . Droz + floss @ gmail DOT com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 */

import { Assets } from './Assets.js';

class Grav {
    constructor() {
        this.assets = Assets;
        this.user = {};
        this.config = {
            get: s => ''
        };
        this.browser = this.get_browser();
        this.language = {
            getActive() {
                return null;
            },

            translate: (args, languages, array_support, html_out) => {
                if (typeof args === 'object') {
                    return args.shift() || '';
                }

                return args || '';
            },
            translateArray: (key, index, languages = null, html_out = false) => {
                return html_out ? `<span class="untranslated">${key}[${index}]</span>` : `${key}[${index}]`;
            }
        };
    };

    get_browser() {
        // https://stackoverflow.com/questions/5916900/h
        var ua=navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if(/trident/i.test(M[1])){
            tem=/\brv[ :]+(\d+)/g.exec(ua) || [];
            return {browser:'IE',version:(tem[1]||'')};
        }
        if(M[1]==='Chrome'){
            tem = ua.match(/\bOPR|Edge\/(\d+)/);
            if(tem!=null)   {return {browser:'Opera', version:tem[1]};}
        }
        M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
        if((tem=ua.match(/version\/(\d+)/i))!=null) {M.splice(1,1,tem[1]);}
        return {
            browser: M[0],
            version: M[1]
        };
    };

    instance() {
        return new Grav();
    }
};

const g = new Grav();

export { g as Grav };
