class Assets {
    addJs(url, options) {
        return `<script src="${url}"></script>`;
    }
    addInlineJs(str) {
        return `<script>${str}</script>`;
    }
    css(url) {
        return `<style src="${url}"></style>`;
    }
    addCss(url) {
        return `<style src="${url}"></style>`;
    }
}

const g = new Assets();

export { g as Assets };
