//
//@package    Grav\Common\Twig
//
//@copyright  Copyright (C) 2015 - 2019 Trilby Media, LLC. All rights reserved.
//@license    MIT License; see LICENSE file for details.
//

//
//Parses a token and returns a node.
//
//@param Token $token A Twig_Token instance
//
//@return Node A Twig_Node instance
//
//
//@param Token $token
//@return array
//
//
//Gets the tag name associated with this token parser.
//
//@return string The tag name
//
export class TwigTokenParserRender extends AbstractTokenParser {
    parse(token) {
        var object, layout, context;
        var lineno = token.getLine();
        [object, layout, context] = this.parseArguments(token);
        return new TwigNodeRender(object, layout, context, lineno, this.getTag());
    }

    parseArguments(token) {
        var stream = this.parser.getStream();
        var object = this.parser.getExpressionParser().parseExpression();
        var layout = undefined;

        if (stream.nextIf(Token.NAME_TYPE, "layout")) {
            stream.expect(Token.PUNCTUATION_TYPE, ":");
            layout = this.parser.getExpressionParser().parseExpression();
        }

        var context = undefined;

        if (stream.nextIf(Token.NAME_TYPE, "with")) {
            context = this.parser.getExpressionParser().parseExpression();
        }

        stream.expect(Token.BLOCK_END_TYPE);
        return [object, layout, context];
    }

    getTag() {
        return "render";
    }

}
