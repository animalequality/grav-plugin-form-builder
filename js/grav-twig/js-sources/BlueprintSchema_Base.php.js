//@var array
//@var array
//@var array
//@var array
//@var array
//@var array
//@var array
//
//Constructor.
//
//@param array $serialized  Serialized content if available.
//
//
//@param array $types
//@return $this
//
//
//Restore Blueprints object.
//
//@param array $serialized
//@return static
//
//
//Initialize blueprints with its dynamic fields.
//
//@return $this
//
//
//Set filter for inherited properties.
//
//@param array $filter     List of field names to be inherited.
//
//
//Get value by using dot notation for nested arrays/objects.
//
//@example $value = $data->get('this.is.my.nested.variable');
//
//@param string  $name       Dot separated path to the requested value.
//@param mixed   $default    Default value (or null).
//@param string  $separator  Separator, defaults to '.'
//
//@return mixed  Value.
//
//
//Set value by using dot notation for nested arrays/objects.
//
//@example $value = $data->set('this.is.my.nested.variable', $newField);
//
//@param string  $name       Dot separated path to the requested value.
//@param mixed   $value      New value.
//@param string  $separator  Separator, defaults to '.'
//
//
//Define value by using dot notation for nested arrays/objects.
//
//@example $value = $data->set('this.is.my.nested.variable', true);
//
//@param string  $name       Dot separated path to the requested value.
//@param mixed   $value      New value.
//@param string  $separator  Separator, defaults to '.'
//
//
//@return array
//@deprecated
//
//
//Convert object into an array.
//
//@return array
//
//
//Get nested structure containing default values defined in the blueprints.
//
//Fields without default value are ignored in the list.
//
//@return array
//
//
//Embed an array to the blueprint.
//
//@param $name
//@param array $value
//@param string $separator
//@param bool $merge   Merge fields instead replacing them.
//@return $this
//
//
//Merge two arrays by using blueprints.
//
//@param  array $data1
//@param  array $data2
//@param  string $name         Optional
//@param  string $separator    Optional
//@return array
//
//
//Get the property with given path.
//
//@param string $path
//@param string $separator
//@return mixed
//
//
//Returns name of the property with given path.
//
//@param string $path
//@param string $separator
//@return string
//
//
//Return data fields that do not exist in blueprints.
//
//@param  array  $data
//@param  string $prefix
//@return array
//
//
//Get the property with given path.
//
//@param $property
//@param $nested
//@return mixed
//
//
//Get property from the definition.
//
//@param  string  $path  Comma separated path to the property.
//@param  string  $separator
//@return array|string|null
//@internal
//
//
//@param array $nested
//@return array
//
//
//@param array $data1
//@param array $data2
//@param array $rules
//@return array
//@internal
//
//
//Gets all field definitions from the blueprints.
//
//@param array  $fields    Fields to parse.
//@param array  $params    Property parameters.
//@param string $prefix    Property prefix.
//@param string $parent    Parent property.
//@param bool   $merge     Merge fields instead replacing them.
//@param array $formPath
//
//
//@param string $key
//@param array $field
//@param array $params
//@param string $prefix
//@param string $parent
//@param bool $merge
//@param array $formPath
//
//
//Add property to the definition.
//
//@param  string  $path  Comma separated path to the property.
//@internal
//
//
//Remove property to the definition.
//
//@param  string  $path  Comma separated path to the property.
//@internal
//
//
//@param $rule
//@return array
//@internal
//
//
//@param array $data
//@param array $rules
//@param string $prefix
//@return array
//@internal
//
//
//@param array $field
//@param string $property
//@param array $call
//
export default class BlueprintSchema {
    constructor(serialized = undefined) {
        this.items = Array();
        this.rules = Array();
        this.nested = Array();
        this.dynamic = Array();
        this.filter = {
            validation: true
        };
        this.ignoreFormKeys = {
            fields: 1
        };
        this.types = Array();

        if (Array.isArray(serialized) && !!serialized) {
            this.items = Array.from(serialized.items || "");
            this.rules = Array.from(serialized.rules || "");
            this.nested = Array.from(serialized.nested || "");
            this.dynamic = Array.from(serialized.dynamic || "");
            this.filter = Array.from(serialized.filter || "");
        }
    }

    setTypes(types) {
        this.types = types;
        return this;
    }

    static restore(serialized) {
        return new static(serialized);
    }

    init() {
        {
            let _tmp_0 = this.dynamic;

            for (var key in _tmp_0) {
                var data = _tmp_0[key];
                var field = this.items[key];

                for (var property in data) {
                    var call = data[property];
                    var action = "dynamic" + ucfirst(call.action);

                    if (method_exists(this, action)) {
                        this[action](field, property, call);
                    }
                }
            }
        }
        return this;
    }

    setFilter(filter) {
        this.filter = array_flip(filter);
    }

    get(name, default_r = undefined, separator = ".") {
        name = separator !== "." ? str_replace(separator, ".", name) : name;
        return undefined !== this.items[name] ? this.items[name] : default_r;
    }

    set(name, value, separator = ".") {
        name = separator !== "." ? str_replace(separator, ".", name) : name;
        this.items[name] = value;
        this.addProperty(name);
    }

    def(name, value, separator = ".") {
        this.set(name, this.get(name, value, separator), separator);
    }

    toArray() {
        return this.getState();
    }

    getState() {
        return {
            items: this.items,
            rules: this.rules,
            nested: this.nested,
            dynamic: this.dynamic,
            filter: this.filter
        };
    }

    getDefaults() {
        return this.buildDefaults(this.nested);
    }

    embed(name, value, separator = ".", merge = false) {
        if (undefined !== value.rules) {
            this.rules = array_merge(this.rules, value.rules);
        }

        name = separator !== "." ? str_replace(separator, ".", name) : name;

        if (undefined !== value.form) {
            var form = array_diff_key(value.form, {
                fields: 1,
                field: 1
            });
        } else {
            form = Array();
        }

        var items = undefined !== this.items[name] ? this.items[name] : {
            type: "_root",
            form_field: false
        };
        this.items[name] = items;
        this.addProperty(name);
        var prefix = name ? name + "." : "";
        var params = array_intersect_key(form, this.filter);
        var location = [name];

        if (undefined !== value.form.field) {
            this.parseFormField(name, value.form.field, params, prefix, "", merge, location);
        } else if (undefined !== value.form.fields) {
            this.parseFormFields(value.form.fields, params, prefix, "", merge, location);
        }

        this.items[name] += {
            form: form
        };
        return this;
    }

    mergeData(data1, data2, name = undefined, separator = ".") {
        var nested = this.getNested(name, separator);

        if (!Array.isArray(nested)) {
            nested = Array();
        }

        return this.mergeArrays(data1, data2, nested);
    }

    getProperty(path = undefined, separator = ".") {
        var name = this.getPropertyName(path, separator);
        var property = this.get(name);
        var nested = this.getNested(name);
        return this.getPropertyRecursion(property, nested);
    }

    getPropertyName(path = undefined, separator = ".") {
        var part;
        var parts = path.split(separator);
        var nested = this.nested;
        var result = Array();

        while ((part = parts.shift()) !== undefined) {
            if (!(undefined !== nested[part])) {
                if (undefined !== nested["*"]) {
                    part = "*";
                } else {
                    return array_merge(result, [part], parts).join(separator);
                }
            }

            result.push(part);
            nested = nested[part];
        }

        return result.join(".");
    }

    extra(data, prefix = "") //Drill down to prefix level
    {
        var rules = this.nested;

        if (!!prefix) {
            var parts = prefix.replace(/^[\.]*|[\.]*$/g, "").split(".");

            for (var part of Object.values(parts)) {
                rules = undefined !== rules[part] ? rules[part] : Array();
            }
        }

        if (undefined !== rules[""]) {
            var rule = this.items[""];

            if (undefined !== rule.type && rule.type !== "_root") {
                return Array();
            }
        }

        return this.extraArray(data, rules, prefix);
    }

    getPropertyRecursion(property, nested) {
        if (!nested || !Array.isArray(nested) || !(undefined !== property.type)) {
            return property;
        }

        if (property.type === "_root") {
            for (var key in nested) {
                var value = nested[key];

                if (key === "") {
                    continue;
                }

                var name = Array.isArray(value) ? key : value;
                property.fields[key] = this.getPropertyRecursion(this.get(name), value);
            }
        } else if (property.type === "_parent" || !!property.array) {
            for (var key in nested) {
                var value = nested[key];
                name = Array.isArray(value) ? `${property.name}.${key}` : value;
                property.fields[key] = this.getPropertyRecursion(this.get(name), value);
            }
        }

        return property;
    }

    getNested(path = undefined, separator = ".") {
        if (!path) {
            return this.nested;
        }

        var parts = path.split(separator);
        var item = parts.pop();
        var nested = this.nested;

        for (var part of Object.values(parts)) {
            if (!(undefined !== nested[part])) {
                var part = "*";

                if (!(undefined !== nested[part])) {
                    return Array();
                }
            }

            nested = nested[part];
        }

        return undefined !== nested[item] ? nested[item] : undefined !== nested["*"] ? nested["*"] : undefined;
    }

    buildDefaults(nested) {
        var defaults = Array();

        for (var key in nested) {
            var value = nested[key];

            if (key === "*") //TODO: Add support for adding defaults to collections.
                {
                    continue;
                }

            if (Array.isArray(value)) //Recursively fetch the items.
                //Only return defaults if there are any.
                {
                    var list = this.buildDefaults(value);

                    if (!!list) {
                        defaults[key] = list;
                    }
                } else //We hit a field; get default from it if it exists.
                //Only return default value if it exists.
                {
                    var item = this.get(value);

                    if (undefined !== item.default) {
                        defaults[key] = item.default;
                    }
                }
        }

        return defaults;
    }

    mergeArrays(data1, data2, rules) {
        for (var key in data2) {
            var field = data2[key];
            var val = undefined !== rules[key] ? rules[key] : undefined;
            var rule = "string" === typeof val ? this.items[val] : undefined;

            if (key in data1 && Array.isArray(data1[key]) && Array.isArray(field) && Array.isArray(val) && !(undefined !== val["*"]) || !!rule.type && strpos(rule.type, "_") === 0) //Array has been defined in blueprints and is not a collection of items.
                {
                    data1[key] = this.mergeArrays(data1[key], field, val);
                } else //Otherwise just take value from the data2.
                {
                    data1[key] = field;
                }
        }

        return data1;
    }

    parseFormFields(fields, params, prefix = "", parent = "", merge = false, formPath = Array()) {
        if (undefined !== fields.type && !Array.isArray(fields.type)) {
            return;
        }

        for (var key in fields) {
            var field = fields[key];
            this.parseFormField(key, field, params, prefix, parent, merge, formPath);
        }
    }

    parseFormField(key, field, params, prefix = "", parent = "", merge = false, formPath = Array()) //Skip illegal field (needs to be an array).
    //Set default properties for the field type.
    {
        if (!Array.isArray(field)) {
            return;
        }

        key = this.getFieldKey(key, prefix, parent);
        var newPath = array_merge(formPath, [key]);
        var properties = array_diff_key(field, this.ignoreFormKeys) + params;
        properties.name = key;
        var type = undefined !== properties.type ? properties.type : "";

        if (undefined !== this.types[type]) {
            properties += this.types[type];
        }

        if (merge && undefined !== this.items[key]) {
            properties += this.items[key];
        }

        var isInputField = !(undefined !== properties["input@"]) || properties["input@"];
        var propertyExists = undefined !== this.items[key];

        if (!isInputField) //Remove property if it exists.
            {
                if (propertyExists) {
                    this.removeProperty(key);
                }
            } else if (!propertyExists) //Add missing property.
            {
                this.addProperty(key);
            }

        if (undefined !== field.fields) //Recursively get all the nested fields.
            {
                var isArray = !!properties.array;
                var newParams = array_intersect_key(properties, this.filter);
                this.parseFormFields(field.fields, newParams, prefix, key + (isArray ? ".*" : ""), merge, newPath);
            } else {
            if (!(undefined !== this.items[key])) //Add parent rules.
                {
                    var path = key.split(".");
                    path.pop();
                    parent = "";

                    for (var part of Object.values(path)) {
                        parent += (parent ? "." : "") + part;

                        if (!(undefined !== this.items[parent])) {
                            this.items[parent] = {
                                type: "_parent",
                                name: parent,
                                form_field: false
                            };
                        }
                    }
                }

            if (isInputField) {
                this.parseProperties(key, properties);
            }
        }

        if (isInputField) {
            this.items[key] = properties;
        }
    }

    getFieldKey(key, prefix, parent) //Set name from the array key.
    {
        if (strpos(key[0], ".") === 0) {
            return (parent ? parent : prefix.replace(/[\.]*$/, "")) + key;
        }

        return prefix + key;
    }

    parseProperties(key, properties) //Initialize predefined validation rule.
    {
        key = key.replace(/^[\.]*/, "");

        if (!!properties.data) {
            this.dynamic[key] = properties.data;
        }

        for (var name in properties) {
            var value = properties[name];

            if (strpos(name[0], "@") !== false) {
                var list = name.replace(/^@*|@*$/g, "").split("-", 2);
                var action = list.shift();
                var property = list.shift();
                this.dynamic[key][property] = {
                    action: action,
                    params: value
                };
            }
        }

        if (undefined !== properties.validate.rule) {
            properties.validate += this.getRule(properties.validate.rule);
        }
    }

    addProperty(path) {
        var parts = path.split(".");
        var item = parts.pop();
        var nested = this.nested;

        for (var part of Object.values(parts)) {
            if (!(undefined !== nested[part]) || !Array.isArray(nested[part])) {
                nested[part] = Array();
            }

            nested = nested[part];
        }

        if (!(undefined !== nested[item])) {
            nested[item] = path;
        }
    }

    removeProperty(path) {
        var parts = path.split(".");
        var item = parts.pop();
        var nested = this.nested;

        for (var part of Object.values(parts)) {
            if (!(undefined !== nested[part]) || !Array.isArray(nested[part])) {
                return;
            }

            nested = nested[part];
        }

        if (undefined !== nested[item]) {
            delete nested[item];
        }
    }

    getRule(rule) {
        if (undefined !== this.rules[rule] && Array.isArray(this.rules[rule])) {
            return this.rules[rule];
        }

        return Array();
    }

    extraArray(data, rules, prefix) {
        var array = Array();

        for (var key in data) {
            var field = data[key];
            var val = undefined !== rules[key] ? rules[key] : undefined !== rules["*"] ? rules["*"] : undefined;
            var rule = "string" === typeof val ? this.items[val] : undefined;

            if (rule || undefined !== val["*"]) //Item has been defined in blueprints.
                {} else if (Array.isArray(field) && Array.isArray(val)) //Array has been defined in blueprints.
                {
                    array += this.extraArray(field, val, prefix + key + ".");
                } else //Undefined/extra item.
                {
                    array[prefix + key] = field;
                }
        }

        return array;
    }

    dynamicData(field, property, call) //If function returns a value,
    {
        var params = call.params;

        if (Array.isArray(params)) {
            var function_r = params.shift();
        } else {
            function_r = params;
            params = Array();
        }

        var list = function_r.split("::", 2);
        var f = list.pop();
        var o = list.pop();

        if (!o) {
            if ("function" === typeof global[f]) {
                var data = call_user_func_array(f, params);
            }
        } else {
            if (method_exists(o, f)) {
                data = call_user_func_array([o, f], params);
            }
        }

        if (undefined !== data) {
            if (Array.isArray(data) && undefined !== field[property] && Array.isArray(field[property])) //Combine field and @data-field together.
                {
                    field[property] += data;
                } else //Or create/replace field with @data-field.
                {
                    field[property] = data;
                }
        }
    }

}
