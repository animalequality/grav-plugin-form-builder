//
//@package    Grav\Common\Data
//
//@copyright  Copyright (C) 2015 - 2019 Trilby Media, LLC. All rights reserved.
//@license    MIT License; see LICENSE file for details.
//

//@var string
//@var BlueprintSchema
//@var array
//
//Set default values for field types.
//
//@param array $types
//@return $this
//
//
//Get nested structure containing default values defined in the blueprints.
//
//Fields without default value are ignored in the list.
//
//@return array
//
//
//Initialize blueprints with its dynamic fields.
//
//@return $this
//
//
//Merge two arrays by using blueprints.
//
//@param  array $data1
//@param  array $data2
//@param  string $name         Optional
//@param  string $separator    Optional
//@return array
//
//
//Process data coming from a form.
//
//@param array $data
//@param array $toggles
//@return array
//
//
//Return data fields that do not exist in blueprints.
//
//@param  array  $data
//@param  string $prefix
//@return array
//
//
//Validate data against blueprints.
//
//@param  array $data
//@throws \RuntimeException
//
//
//Filter data by using blueprints.
//
//@param  array $data
//@param  bool $missingValuesAsNull
//@param  bool $keepEmptyValues
//@return array
//
//
//Flatten data by using blueprints.
//
//@param  array $data
//@return array
//
//
//Return blueprint data schema.
//
//@return BlueprintSchema
//
//
//Initialize validator.
//
//
//@param string $filename
//@return string
//
//
//@param string|array $path
//@param string $context
//@return array
//
//
//@param array $field
//@param string $property
//@param array $call
//
//
//@param array $field
//@param string $property
//@param array $call
//
//
//@param array $field
//@param string $property
//@param array $call
//
//
//@param array $field
//@param string $property
//@param array $call
//
export default class Blueprint extends BlueprintForm {
    constructor() {
        super(...arguments);
        this.context = "blueprints://";
        this.handlers = Array();
    }

    __clone() {
        if (this.blueprintSchema) {
            this.blueprintSchema = clone(this.blueprintSchema);
        }
    }

    setScope(scope) {
        this.scope = scope;
    }

    setTypes(types) {
        this.initInternals();
        this.blueprintSchema.setTypes(types);
        return this;
    }

    getDefaults() {
        this.initInternals();

        if (undefined === this.defaults) {
            this.defaults = this.blueprintSchema.getDefaults();
        }

        return this.defaults;
    }

    init() {
        {
            let _tmp_0 = this.dynamic;

            for (var key in _tmp_0) //Locate field.
            //Set dynamic property.
            {
                var data = _tmp_0[key];
                var path = key.split("/");
                var current = this.items;

                for (var field of Object.values(path)) {
                    if ("object" === typeof current) //Handle objects.
                        {
                            if (!(undefined !== current[field])) {
                                current[field] = Array();
                            }

                            current = current[field];
                        } else //Handle arrays and scalars.
                        {
                            if (!Array.isArray(current)) {
                                current = {
                                    [field]: Array()
                                };
                            } else if (!(undefined !== current[field])) {
                                current[field] = Array();
                            }

                            current = current[field];
                        }
                }

                for (var property in data) {
                    var call = data[property];
                    var action = call.action;
                    var method = "dynamic" + ucfirst(action);

                    if (undefined !== this.handlers[action]) {
                        var callable = this.handlers[action];
                        callable(current, property, call);
                    } else if (method_exists(this, method)) {
                        this[method](current, property, call);
                    }
                }
            }
        }
        return this;
    }

    mergeData(data1, data2, name = undefined, separator = ".") {
        this.initInternals();
        return this.blueprintSchema.mergeData(data1, data2, name, separator);
    }

    processForm(data, toggles = Array()) {
        this.initInternals();
        return this.blueprintSchema.processForm(data, toggles);
    }

    extra(data, prefix = "") {
        this.initInternals();
        return this.blueprintSchema.extra(data, prefix);
    }

    validate(data) {
        this.initInternals();
        this.blueprintSchema.validate(data);
    }

    filter(data, missingValuesAsNull = false, keepEmptyValues = false) {
        this.initInternals();
        return this.blueprintSchema.filter(data, missingValuesAsNull, keepEmptyValues);
    }

    flattenData(data) {
        this.initInternals();
        return this.blueprintSchema.flattenData(data);
    }

    schema() {
        this.initInternals();
        return this.blueprintSchema;
    }

    addDynamicHandler(name, callable) {
        this.handlers[name] = callable;
    }

    initInternals() {
        if (undefined === this.blueprintSchema) {
            var types = Grav.instance().plugins.formFieldTypes;
            this.blueprintSchema = new BlueprintSchema();

            if (types) {
                this.blueprintSchema.setTypes(types);
            }

            this.blueprintSchema.embed("", this.items);
            this.blueprintSchema.init();
            this.defaults = undefined;
        }
    }

    loadFile(filename) {
        var file = CompiledYamlFile.instance(filename);
        var content = file.content();
        file.free();
        return content;
    }

    getFiles(path, context = undefined) //@var UniformResourceLocator $locator
    {
        var locator = Grav.instance().locator;

        if ("string" === typeof path && !locator.isStream(path)) //Find path overrides.
            //Add path pointing to default context.
            {
                var paths = Array.from(this.overrides[path] || undefined || "");

                if (context === undefined) {
                    context = this.context;
                }

                if (context && context[context.length - 1] !== "/") {
                    context += "/";
                }

                path = context + path;

                if (!preg_match("/\\.yaml$/", path)) {
                    path += ".yaml";
                }

                paths.push(path);
            } else {
            paths = Array.from(path || "");
        }

        var files = Array();

        for (var lookup of Object.values(paths)) {
            if ("string" === typeof lookup && strpos(lookup, "://")) {
                files = array_merge(files, locator.findResources(lookup));
            } else {
                files.push(lookup);
            }
        }

        return Object.values(Array.from(new Set(files)));
    }

    dynamicData(field, property, call) //If function returns a value,
    {
        var params = call.params;

        if (Array.isArray(params)) {
            var function_r = params.shift();
        } else {
            function_r = params;
            params = Array();
        }

        [o, f] = function_r.split("::", 2);
        var data = undefined;

        if (!f) {
            if ("function" === typeof global[o]) {
                data = call_user_func_array(o, params);
            }
        } else {
            if (method_exists(o, f)) {
                data = call_user_func_array([o, f], params);
            }
        }

        if (undefined !== data) {
            if (Array.isArray(data) && undefined !== field[property] && Array.isArray(field[property])) //Combine field and @data-field together.
                {
                    field[property] += data;
                } else //Or create/replace field with @data-field.
                {
                    field[property] = data;
                }
        }
    }

    dynamicConfig(field, property, call) {
        var value = call.params;
        var default_r = field[property] || undefined;
        var config = Grav.instance().config.get(value, default_r);

        if (undefined !== config) {
            field[property] = config;
        }
    }

    dynamicSecurity(field, property, call) //@var UserInterface $user
    {
        if (property || !!field.validate.ignore) {
            return;
        }

        var grav = Grav.instance();
        var actions = Array.from(call.params || "");

        if (undefined !== grav.user) {
            var user = Grav.instance().user;

            for (var action of Object.values(actions)) {
                if (!user.authorize(action)) {
                    this.addPropertyRecursive(field, "validate", {
                        ignore: true
                    });
                    return;
                }
            }
        }
    }

    dynamicScope(field, property, call) {
        if (property && property !== "ignore") {
            return;
        }

        var scopes = Array.from(call.params || "");
        var matches = -1 !== scopes.indexOf(this.scope);

        if (this.scope && property !== "ignore") {
            matches = !matches;
        }

        if (matches) {
            this.addPropertyRecursive(field, "validate", {
                ignore: true
            });
            return;
        }
    }

    addPropertyRecursive(field, property, value) {
        if (Array.isArray(value) && undefined !== field[property] && Array.isArray(field[property])) {
            field[property] = array_merge_recursive(field[property], value);
        } else {
            field[property] = value;
        }

        if (!!field.fields) {
            {
                let _tmp_1 = field.fields;

                for (var key in _tmp_1) {
                    var child = _tmp_1[key];
                    this.addPropertyRecursive(child, property, value);
                }
            }
        }
    }

}
