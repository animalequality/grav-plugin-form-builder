//
//@package    Grav\Common\Data
//
//@copyright  Copyright (C) 2015 - 2019 Trilby Media, LLC. All rights reserved.
//@license    MIT License; see LICENSE file for details.
//

//
//@return array
//
//
//@param string $name
//@return array
//
//
//Validate data against blueprints.
//
//@param  array $data
//@throws \RuntimeException
//
//
//@param array $data
//@param array $toggles
//@return array
//
//
//Filter data by using blueprints.
//
//@param  array $data                  Incoming data, for example from a form.
//@param  bool  $missingValuesAsNull   Include missing values as nulls.
//@param bool   $keepEmptyValues       Include empty values.
//@return array
//
//
//Flatten data by using blueprints.
//
//@param  array $data                  Data to be flattened.
//@return array
//
//
//@param array $data
//@param array $rules
//@param string $prefix
//@return array
//
//
//@param array $data
//@param array $rules
//@return array
//@throws \RuntimeException
//
//
//@param array $data
//@param array $rules
//@param bool  $missingValuesAsNull
//@param bool $keepEmptyValues
//@return array
//
//
//@param array|null $data
//@param array $toggles
//@param array $nested
//@return array|null
//
//
//@param array $data
//@param array $fields
//@return array
//
//
//@param array $field
//@param string $property
//@param array $call
//
export default class BlueprintSchema extends BlueprintSchemaBase {
    constructor() {
        super(...arguments);
        this.ignoreFormKeys = {
            title: true,
            help: true,
            placeholder: true,
            placeholder_key: true,
            placeholder_value: true,
            fields: true
        };
    }

    getTypes() {
        return this.types;
    }

    getType(name) {
        return this.types[name] || Array();
    }

    validate(data) {
        try {
            var messages = this.validateArray(data, this.nested);
        } catch (e) {
            if (e instanceof RuntimeException) {
                throw new ValidationException(e.getMessage(), e.getCode(), e).setMessages();
            }
        }

        if (!!messages) {
            throw new ValidationException().setMessages(messages);
        }
    }

    processForm(data, toggles = Array()) {
        return this.processFormRecursive(data, toggles, this.nested);
    }

    filter(data, missingValuesAsNull = false, keepEmptyValues = false) {
        return this.filterArray(data, this.nested, missingValuesAsNull, keepEmptyValues);
    }

    flattenData(data) {
        return this.flattenArray(data, this.nested, "");
    }

    flattenArray(data, rules, prefix) {
        var array = Array();

        for (var key in data) {
            var field = data[key];
            var val = rules[key] || rules["*"] || undefined;
            var rule = "string" === typeof val ? this.items[val] : undefined;

            if (rule || undefined !== val["*"]) //Item has been defined in blueprints.
                {
                    array[prefix + key] = field;
                } else if (Array.isArray(field) && Array.isArray(val)) //Array has been defined in blueprints.
                {
                    array += this.flattenArray(field, val, prefix + key + ".");
                } else //Undefined/extra item.
                {
                    array[prefix + key] = field;
                }
        }

        return array;
    }

    validateArray(data, rules) {
        var messages = this.checkRequired(data, rules);

        for (var key in data) {
            var field = data[key];
            var val = rules[key] || rules["*"] || undefined;
            var rule = "string" === typeof val ? this.items[val] : undefined;

            if (rule) //Item has been defined in blueprints.
                {
                    if (!!rule.disabled || !!rule.validate.ignore) //Skip validation in the ignored field.
                        {
                            continue;
                        }

                    messages += Validation.validate(field, rule);
                } else if (Array.isArray(field) && Array.isArray(val)) //Array has been defined in blueprints.
                {
                    messages += this.validateArray(field, val);
                } else if (undefined !== rules.validation && rules.validation === "strict") //Undefined/extra item.
                {
                    throw new RuntimeException(sprintf("%s is not defined in blueprints", key));
                }
        }

        return messages;
    }

    filterArray(data, rules, missingValuesAsNull, keepEmptyValues) {
        var results = Array();

        if (missingValuesAsNull) //First pass is to fill up all the fields with null. This is done to lock the ordering of the fields.
            {
                for (var key in rules) {
                    var rule = rules[key];

                    if (key && !(undefined !== results[key])) {
                        var val = rules[key] || rules["*"] || undefined;
                        var rule = "string" === typeof val ? this.items[val] : undefined;

                        if (!rule.disabled && !rule.validate.ignore) {
                            continue;
                        }
                    }
                }
            }

        for (var key in data) {
            var field = data[key];
            val = rules[key] || rules["*"] || undefined;
            rule = "string" === typeof val ? this.items[val] : undefined;

            if (rule) //Item has been defined in blueprints.
                {
                    if (!!rule.disabled || !!rule.validate.ignore) //Skip any data in the ignored field.
                        {
                            delete results[key];
                            continue;
                        }

                    var field = Validation.filter(field, rule);
                } else if (Array.isArray(field) && Array.isArray(val)) //Array has been defined in blueprints.
                {
                    field = this.filterArray(field, val, missingValuesAsNull, keepEmptyValues);
                } else if (undefined !== rules.validation && rules.validation === "strict") //Skip any extra data.
                {
                    continue;
                }

            if (keepEmptyValues || undefined !== field && (!Array.isArray(field) || !!field)) {
                results[key] = field;
            }
        }

        return results ? results : undefined;
    }

    processFormRecursive(data, toggles, nested) {
        for (var key in nested) {
            var value = nested[key];

            if (key === "") {
                continue;
            }

            if (key === "*") //TODO: Add support to collections.
                {
                    continue;
                }

            if (Array.isArray(value)) //Recursively fetch the items.
                {
                    data[key] = this.processFormRecursive(data[key] || undefined, toggles[key] || Array(), value);
                } else //Do not add the field if:
                {
                    var field = this.get(value);

                    if (!field || !!field.disabled || !!field.validate.ignore || !!field.toggleable && !toggles[key]) {
                        continue;
                    }

                    if (!(undefined !== data[key])) {
                        data[key] = undefined;
                    }
                }
        }

        return data;
    }

    checkRequired(data, fields) {
        var messages = Array();

        for (var name in fields) //Skip ignored field, it will not be required.
        {
            var field = fields[name];

            if (!("string" === typeof field)) {
                continue;
            }

            var field = this.items[field];

            if (!!field.disabled || !!field.validate.ignore) {
                continue;
            }

            if (undefined !== field.validate.required && field.validate.required === true) {
                if (undefined !== data[name]) {
                    continue;
                }

                if (field.type === "file" && undefined !== data.data.name[name]) //handle case of file input fields required
                    {
                        continue;
                    }

                var value = field.label || field.name;
                var language = Grav.instance().language;
                var message = sprintf(language.translate("GRAV.FORM.MISSING_REQUIRED_FIELD", undefined, true) + " %s", language.translate(value));
                messages[field.name].push(message);
            }
        }

        return messages;
    }

    dynamicConfig(field, property, call) {
        var value = call.params;
        var default_r = field[property] || undefined;
        var config = Grav.instance().config.get(value, default_r);

        if (undefined !== config) {
            field[property] = config;
        }
    }

}
