//
//@package    Grav\Common\Data
//
//@copyright  Copyright (C) 2015 - 2019 Trilby Media, LLC. All rights reserved.
//@license    MIT License; see LICENSE file for details.
//

//@var array|string
//@var array
//@var array
//
//@param  string|array  $search  Search path.
//
//
//Get blueprint.
//
//@param  string  $type  Blueprint type.
//@return Blueprint
//@throws \RuntimeException
//
//
//Get all available blueprint types.
//
//@return  array  List of type=>name
//
//
//Load blueprint file.
//
//@param  string  $name  Name of the blueprint.
//@return Blueprint
//
class Blueprints {
    constructor(search = "blueprints://") {
        this.instances = Array();
        this.search = search;
    }

    get(type) {
        if (!(undefined !== this.instances[type])) {
            this.instances[type] = this.loadFile(type);
        }

        return this.instances[type];
    }

    types() {
        if (this.types === undefined) //@var UniformResourceLocator $locator
            //Get stream / directory iterator.
            //@var \DirectoryIterator $file
            {
                this.types = Array();
                var grav = Grav.instance();
                var locator = grav.locator;

                if (locator.isStream(this.search)) {
                    var iterator = locator.getIterator(this.search);
                } else {
                    iterator = new DirectoryIterator(this.search);
                }

                for (var file of Object.values(iterator)) {
                    if (!file.isFile() || "." + file.getExtension() !== YAML_EXT) {
                        continue;
                    }

                    var name = file.getBasename(YAML_EXT);
                    this.types[name] = ucfirst(str_replace("_", " ", name));
                }
            }

        return this.types;
    }

    loadFile(name) {
        var blueprint = new Blueprint(name);

        if (Array.isArray(this.search) || "object" === typeof this.search) //Page types.
            {
                blueprint.setOverrides(this.search);
                blueprint.setContext("blueprints://pages");
            } else {
            blueprint.setContext(this.search);
        }

        return blueprint.load().init();
    }

}
