//
//@package    Grav\Common\Twig
//
//@copyright  Copyright (C) 2015 - 2019 Trilby Media, LLC. All rights reserved.
//@license    MIT License; see LICENSE file for details.
//

//
//Parses a token and returns a node.
//
//@param Token $token A Twig_Token instance
//
//@return Node A Twig_Node instance
//
//
//@param Token $token
//@return array
//
//
//@param Token $token
//@return bool
//
//
//Gets the tag name associated with this token parser.
//
//@return string The tag name
//
export class TwigTokenParserScript extends AbstractTokenParser {
    parse(token) {
        var file, group, priority, attributes;
        var lineno = token.getLine();
        var stream = this.parser.getStream();
        [file, group, priority, attributes] = this.parseArguments(token);
        var content = undefined;

        if (file === undefined) {
            content = this.parser.subparse([this, "decideBlockEnd"], true);
            stream.expect(Token.BLOCK_END_TYPE);
        }

        return new TwigNodeScript(content, file, group, priority, attributes, lineno, this.getTag());
    }

    parseArguments(token) {
        var stream = this.parser.getStream();
        var file = undefined;

        if (!stream.test(Token.NAME_TYPE) && !stream.test(Token.OPERATOR_TYPE) && !stream.test(Token.BLOCK_END_TYPE)) {
            file = this.parser.getExpressionParser().parseExpression();
        }

        var group = undefined;

        if (stream.nextIf(Token.OPERATOR_TYPE, "in")) {
            group = this.parser.getExpressionParser().parseExpression();
        }

        var priority = undefined;

        if (stream.nextIf(Token.NAME_TYPE, "priority")) {
            stream.expect(Token.PUNCTUATION_TYPE, ":");
            priority = this.parser.getExpressionParser().parseExpression();
        }

        var attributes = undefined;

        if (stream.nextIf(Token.NAME_TYPE, "with")) {
            attributes = this.parser.getExpressionParser().parseExpression();
        }

        stream.expect(Token.BLOCK_END_TYPE);
        return [file, group, priority, attributes];
    }

    decideBlockEnd(token) {
        return token.test("endscript");
    }

    getTag() {
        return "script";
    }

}
