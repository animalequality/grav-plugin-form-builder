//
//@package    Grav\Common\Twig
//
//@copyright  Copyright (C) 2015 - 2019 Trilby Media, LLC. All rights reserved.
//@license    MIT License; see LICENSE file for details.
//@origin     https://gist.github.com/maxgalbu/9409182
//

//
//{@inheritdoc}
//
//
//Decide if current token marks switch logic.
//
//@param Token $token
//@return bool
//
//
//Decide if current token marks end of swtich block.
//
//@param Token $token
//@return bool
//
//
//{@inheritdoc}
//
export class TwigTokenParserSwitch extends AbstractTokenParser {
    parse(token) //There can be some whitespace between the {% switch %} and first {% case %} tag.
    {
        var lineno = token.getLine();
        var stream = this.parser.getStream();
        var name = this.parser.getExpressionParser().parseExpression();
        stream.expect(Token.BLOCK_END_TYPE);

        while (stream.getCurrent().getType() === Token.TEXT_TYPE && stream.getCurrent().getValue().trim() === "") {
            stream.next();
        }

        stream.expect(Token.BLOCK_START_TYPE);
        var expressionParser = this.parser.getExpressionParser();
        var default_r = undefined;
        var cases = Array();
        var end = false;

        while (!end) {
            var next = stream.next();

            switch (next.getValue()) {
                case "case":
                    var values = Array();

                    while (true) //Multiple allowed values?
                    {
                        values.push(expressionParser.parsePrimaryExpression());

                        if (stream.test(Token.OPERATOR_TYPE, "or")) {
                            stream.next();
                        } else {
                            break;
                        }
                    }

                    stream.expect(Token.BLOCK_END_TYPE);
                    var body = this.parser.subparse([this, "decideIfFork"]);
                    cases.push(new Node({
                        values: new Node(values),
                        body: body
                    }));
                    break;

                case "default":
                    stream.expect(Token.BLOCK_END_TYPE);
                    default_r = this.parser.subparse([this, "decideIfEnd"]);
                    break;

                case "endswitch":
                    end = true;
                    break;

                default:
                    throw new SyntaxError(sprintf("Unexpected end of template. Twig was looking for the following tags \"case\", \"default\", or \"endswitch\" to close the \"switch\" block started at line %d)", lineno), -1);
            }
        }

        stream.expect(Token.BLOCK_END_TYPE);
        return new TwigNodeSwitch(name, new Node(cases), default_r, lineno, this.getTag());
    }

    decideIfFork(token) {
        return token.test(["case", "default", "endswitch"]);
    }

    decideIfEnd(token) {
        return token.test(["endswitch"]);
    }

    getTag() {
        return "switch";
    }

}
