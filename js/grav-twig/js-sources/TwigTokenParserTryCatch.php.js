//
//@package    Grav\Common\Twig
//
//@copyright  Copyright (C) 2015 - 2019 Trilby Media, LLC. All rights reserved.
//@license    MIT License; see LICENSE file for details.
//

//
//Parses a token and returns a node.
//
//@param Token $token A Twig_Token instance
//
//@return Node A Twig_Node instance
//
//
//Gets the tag name associated with this token parser.
//
//@return string The tag name
//
export class TwigTokenParserTryCatch extends AbstractTokenParser {
    parse(token) {
        var lineno = token.getLine();
        var stream = this.parser.getStream();
        stream.expect(Token.BLOCK_END_TYPE);
        var try_r = this.parser.subparse([this, "decideCatch"]);
        stream.next();
        stream.expect(Token.BLOCK_END_TYPE);
        var catch_r = this.parser.subparse([this, "decideEnd"]);
        stream.next();
        stream.expect(Token.BLOCK_END_TYPE);
        return new TwigNodeTryCatch(try_r, catch_r, lineno, this.getTag());
    }

    decideCatch(token) {
        return token.test(["catch"]);
    }

    decideEnd(token) {
        return token.test(["endtry"]) || token.test(["endcatch"]);
    }

    getTag() {
        return "try";
    }

}
