//
//@package    Grav\Common\Twig
//
//@copyright  Copyright (C) 2015 - 2019 Trilby Media, LLC. All rights reserved.
//@license    MIT License; see LICENSE file for details.
//

//
//{@inheritdoc}
//
//
//Decide if current token marks end of Markdown block.
//
//@param Token $token
//@return bool
//
//
//{@inheritdoc}
//
export class TwigTokenParserMarkdown extends AbstractTokenParser {
  parse(token) {
    var lineno = token.getLine();
    this.parser.getStream().expect(Token.BLOCK_END_TYPE);
    var body = this.parser.subparse([this, "decideMarkdownEnd"], true);
    this.parser.getStream().expect(Token.BLOCK_END_TYPE);
    return new TwigNodeMarkdown(body, lineno, this.getTag());
  }

  decideMarkdownEnd(token) {
    return token.test("endmarkdown");
  }

  getTag() {
    return "markdown";
  }

}
