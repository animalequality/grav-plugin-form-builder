//
//@package    Grav\Common\Twig
//
//@copyright  Copyright (C) 2015 - 2019 Trilby Media, LLC. All rights reserved.
//@license    MIT License; see LICENSE file for details.
//

//
//@param AbstractExpression $object
//@param AbstractExpression|null $layout
//@param AbstractExpression|null $context
//@param int $lineno
//@param string|null $tag
//
//
//Compiles the node to PHP.
//
//@param Compiler $compiler A Twig_Compiler instance
//@throws \LogicException
//
export class TwigNodeRender extends Node {
    constructor(object, layout, context, lineno, tag = undefined) {
        super({
            object: object,
            layout: layout,
            context: context
        }, Array(), lineno, tag);
        this.tagName = "render";
    }

    compile(compiler) {
        compiler.addDebugInfo(this);
        compiler.write("$object = ").subcompile(this.getNode("object")).raw(";" + PHP_EOL);
        var layout = this.getNode("layout");

        if (layout) {
            compiler.write("$layout = ").subcompile(layout).raw(";" + PHP_EOL);
        } else {
            compiler.write("$layout = null;" + PHP_EOL);
        }

        var context = this.getNode("context");

        if (context) {
            compiler.write("$attributes = ").subcompile(context).raw(";" + PHP_EOL);
        } else {
            compiler.write("$attributes = null;" + PHP_EOL);
        }

        compiler.write("$html = $object->render($layout, $attributes ?? []);" + PHP_EOL).write("$block = $context['block'] ?? null;" + PHP_EOL).write("if ($block instanceof \\Grav\\Framework\\ContentBlock\\ContentBlock && $html instanceof \\Grav\\Framework\\ContentBlock\\ContentBlock) {" + PHP_EOL).indent().write("$block->addBlock($html);" + PHP_EOL).write("echo $html->getToken();" + PHP_EOL).outdent().write("} else {" + PHP_EOL).indent().write("echo (string)$html;" + PHP_EOL).outdent().write("}" + PHP_EOL);
    }

}
