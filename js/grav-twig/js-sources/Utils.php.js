//
//@package    Grav\Common
//
//@copyright  Copyright (C) 2015 - 2019 Trilby Media, LLC. All rights reserved.
//@license    MIT License; see LICENSE file for details.
//

//^((?:http[s]?:)?[\/]?(?:\/))
//
//Simple helper method to make getting a Grav URL easier
//
//@param string $input
//@param bool $domain
//@return bool|null|string
//
//
//Check if the $haystack string starts with the substring $needle
//
//@param  string $haystack
//@param  string|string[] $needle
//@param bool $case_sensitive
//
//@return bool
//
//
//Check if the $haystack string ends with the substring $needle
//
//@param  string $haystack
//@param  string|string[] $needle
//@param bool $case_sensitive
//
//@return bool
//
//
//Check if the $haystack string contains the substring $needle
//
//@param  string $haystack
//@param  string|string[] $needle
//@param  bool $case_sensitive
//
//@return bool
//
//
//Function that can match wildcards
//
//match_wildcard('foo*', $test),      // TRUE
//match_wildcard('bar*', $test),      // FALSE
//match_wildcard('*bar*', $test),     // TRUE
//match_wildcard('**blob**', $test),  // TRUE
//match_wildcard('*a?d*', $test),     // TRUE
//match_wildcard('*etc**', $test)     // TRUE
//
//@param string $wildcard_pattern
//@param string $haystack
//@return false|int
//
//
//Returns the substring of a string up to a specified needle.  if not found, return the whole haystack
//
//@param string $haystack
//@param string $needle
//@param bool $case_sensitive
//
//@return string
//
//
//Utility method to replace only the first occurrence in a string
//
//@param string $search
//@param string $replace
//@param string $subject
//
//@return string
//
//
//Utility method to replace only the last occurrence in a string
//
//@param string $search
//@param string $replace
//@param string $subject
//@return string
//
//
//Multibyte compatible substr_replace
//
//@param string $original
//@param string $replacement
//@param int $position
//@param int $length
//@return string
//
//
//Merge two objects into one.
//
//@param  object $obj1
//@param  object $obj2
//
//@return object
//
//
//Recursive Merge with uniqueness
//
//@param array $array1
//@param array $array2
//@return array
//
//
//Returns an array with the differences between $array1 and $array2
//
//@param array $array1
//@param array $array2
//@return array
//
//
//Array combine but supports different array lengths
//
//@param  array $arr1
//@param  array $arr2
//@return array|false
//
//
//Array is associative or not
//
//@param  array $arr
//@return bool
//
//
//Return the Grav date formats allowed
//
//@return array
//
//
//Get current date/time
//
//@param string|null $default_format
//@return string
//@throws \Exception
//
//
//Truncate text by number of characters but can cut off words.
//
//@param  string $string
//@param  int    $limit       Max number of characters.
//@param  bool   $up_to_break truncate up to breakpoint after char count
//@param  string $break       Break point.
//@param  string $pad         Appended padding to the end of the string.
//
//@return string
//
//
//Truncate text by number of characters in a "word-safe" manor.
//
//@param string $string
//@param int    $limit
//
//@return string
//
//
//Truncate HTML by number of characters. not "word-safe"!
//
//@param  string $text
//@param  int $length in characters
//@param  string $ellipsis
//
//@return string
//
//
//Truncate HTML by number of characters in a "word-safe" manor.
//
//@param  string $text
//@param  int    $length in words
//@param  string $ellipsis
//
//@return string
//
//
//Generate a random string of a given length
//
//@param int $length
//
//@return string
//
//
//Provides the ability to download a file to the browser
//
//@param string $file the full path to the file to be downloaded
//@param bool $force_download as opposed to letting browser choose if to download or render
//@param int $sec      Throttling, try 0.1 for some speed throttling of downloads
//@param int $bytes    Size of chunks to send in bytes. Default is 1024
//@throws \Exception
//
//
//Return the mimetype based on filename extension
//
//@param string $extension Extension of file (eg "txt")
//@param string $default
//
//@return string
//
//
//Get all the mimetypes for an array of extensions
//
//@param array $extensions
//@return array
//
//
//Return the mimetype based on filename extension
//
//@param string $mime mime type (eg "text/html")
//@param string $default default value
//
//@return string
//
//
//Get all the extensions for an array of mimetypes
//
//@param array $mimetypes
//@return array
//
//
//Return the mimetype based on filename
//
//@param string $filename Filename or path to file
//@param string $default default value
//
//@return string
//
//
//Return the mimetype based on existing local file
//
//@param string $filename Path to the file
//
//@return string|bool
//
//
//Returns true if filename is considered safe.
//
//@param string $filename
//@return bool
//
//
//Normalize path by processing relative `.` and `..` syntax and merging path
//
//@param string $path
//
//@return string
//
//
//Check whether a function is disabled in the PHP settings
//
//@param string $function the name of the function to check
//
//@return bool
//
//
//Get the formatted timezones list
//
//@return array
//
//
//Recursively filter an array, filtering values by processing them through the $fn function argument
//
//@param array    $source the Array to filter
//@param callable $fn     the function to pass through each array item
//
//@return array
//
//
//Flatten an array
//
//@param array $array
//@return array
//
//
//Flatten a multi-dimensional associative array into dot notation
//
//@param  array   $array
//@param  string  $prepend
//@return array
//
//
//Opposite of flatten, convert flat dot notation array to multi dimensional array
//
//@param array $array
//@param string $separator
//@return array
//
//
//Checks if the passed path contains the language code prefix
//
//@param string $string The path
//
//@return bool
//
//
//
//Get the timestamp of a date
//
//@param string $date a String expressed in the system.pages.dateformat.default format, with fallback to a
//strtotime argument
//@param string $format a date format to use if possible
//@return int the timestamp
//
//
//@param array $array
//@param string $path
//@param null $default
//@return mixed
//
//@deprecated 1.5 Use ->getDotNotation() method instead.
//
//
//Checks if a value is positive
//
//@param string $value
//
//@return boolean
//
//
//Generates a nonce string to be hashed. Called by self::getNonce()
//We removed the IP portion in this version because it causes too many inconsistencies
//with reverse proxy setups.
//
//@param string $action
//@param bool   $previousTick if true, generates the token for the previous tick (the previous 12 hours)
//
//@return string the nonce string
//
//
//Get the time-dependent variable for nonce creation.
//
//Now a tick lasts a day. Once the day is passed, the nonce is not valid any more. Find a better way
//to ensure nonces issued near the end of the day do not expire in that small amount of time
//
//@return int the time part of the nonce. Changes once every 24 hours
//
//
//Creates a hashed nonce tied to the passed action. Tied to the current user and time. The nonce for a given
//action is the same for 12 hours.
//
//@param string $action      the action the nonce is tied to (e.g. save-user-admin or move-page-homepage)
//@param bool   $previousTick if true, generates the token for the previous tick (the previous 12 hours)
//
//@return string the nonce
//
//
//Verify the passed nonce for the give action
//
//@param string|string[] $nonce  the nonce to verify
//@param string $action the action to verify the nonce to
//
//@return boolean verified or not
//
//
//Simple helper method to get whether or not the admin plugin is active
//
//@return bool
//
//
//Get a portion of an array (passed by reference) with dot-notation key
//
//@param array $array
//@param string|int $key
//@param null $default
//@return mixed
//
//
//Set portion of array (passed by reference) for a dot-notation key
//and set the value
//
//@param array      $array
//@param string|int $key
//@param mixed      $value
//@param bool       $merge
//
//@return mixed
//
//
//Utility method to determine if the current OS is Windows
//
//@return bool
//
//
//Utility to determine if the server running PHP is Apache
//
//@return bool
//
//
//Sort a multidimensional array  by another array of ordered keys
//
//@param array $array
//@param array $orderArray
//@return array
//
//
//Sort an array by a key value in the array
//
//@param mixed $array
//@param string|int $array_key
//@param int $direction
//@param int $sort_flags
//@return array
//
//
//Get's path based on a token
//
//@param string $path
//@param PageInterface|null $page
//@return string
//@throws \RuntimeException
//
//
//Convert bytes to the unit specified by the $to parameter.
//
//@param int $bytes The filesize in Bytes.
//@param string $to The unit type to convert to. Accepts K, M, or G for Kilobytes, Megabytes, or Gigabytes, respectively.
//@param int $decimal_places The number of decimal places to return.
//
//@return int Returns only the number of units, not the type letter. Returns 0 if the $to unit type is out of scope.
//
//
//
//Return a pretty size based on bytes
//
//@param int $bytes
//@param int $precision
//@return string
//
//
//Parse a readable file size and return a value in bytes
//
//@param string|int $size
//@return int
//
//
//Multibyte-safe Parse URL function
//
//@param string $url
//@return array
//@throws \InvalidArgumentException
//
//
//Process a string as markdown
//
//@param string $string
//
//@param bool $block  Block or Line processing
//@return string
//
//
//Find the subnet of an ip with CIDR prefix size
//
//@param string $ip
//@param int $prefix
//
//@return string
//@throws \InvalidArgumentException if provided an invalid IP
//
export default class Utils {

    static url(input, domain = false) {
        if (!String(input).trim()) {
            input = "/";
        }

        if (Grav.instance().config.get("system.absolute_urls", false)) {
            domain = true;
        }

        if (Grav.instance().uri.isExternal(input)) {
            return input;
        }

        var uri = Grav.instance().uri;
        var root = uri.rootUrl();
        input = Utils.replaceFirstOccurrence(root, "", input);
        input = String(input).replace(/^[\/]*/, "");

        if (Utils.contains(String(input), "://")) //@var UniformResourceLocator $locator
            {
                var locator = Grav.instance().locator;
                var parts = Uri.parseUrl(input);

                if (parts) {
                    var resource = locator.findResource(`${parts.scheme}://${parts.host}${parts.path}`, false);

                    if (undefined !== parts.query) {
                        resource = resource + "?" + parts.query;
                    }
                } else //Not a valid URL (can still be a stream).
                    {
                        resource = locator.findResource(input, false);
                    }
            } else {
            resource = input;
        }

        return uri.rootUrl(domain).replace(/[\/]*$/, "") + "/" + (resource || "");
    }

    static startsWith(haystack, needle, case_sensitive = true) {
        var status = false;
        var compare_func = case_sensitive ? "mb_strpos" : "mb_stripos";

        for (var each_needle of Object.values(Array.from(needle || ""))) {
            status = each_needle === "" || compare_func(haystack, each_needle) === 0;

            if (status) {
                break;
            }
        }

        return status;
    }

    static endsWith(haystack, needle, case_sensitive = true) {
        var status = false;
        var compare_func = case_sensitive ? "mb_strrpos" : "mb_strripos";

        for (var each_needle of Object.values(Array.from(needle || ""))) {
            var expectedPosition = mb_strlen(haystack) - mb_strlen(each_needle);
            status = each_needle === "" || compare_func(haystack, each_needle, 0) === expectedPosition;

            if (status) {
                break;
            }
        }

        return status;
    }

    static contains(haystack, needle, case_sensitive = true) {
        var status = false;
        var compare_func = case_sensitive ? "mb_strpos" : "mb_stripos";

        for (var each_needle of Object.values(Array.from(needle || ""))) {
            status = each_needle === "" || compare_func(haystack, each_needle) !== false;

            if (status) {
                break;
            }
        }

        return status;
    }

    static matchWildcard(wildcard_pattern, haystack) {
        var regex = str_replace(["\\*", "\\?"], [".*", "."], preg_quote(wildcard_pattern, "/"));
        return preg_match("/^" + regex + "$/is", haystack);
    }

    static substrToString(haystack, needle, case_sensitive = true) {
        var compare_func = case_sensitive ? "mb_strpos" : "mb_stripos";

        if (this.contains(haystack, needle, case_sensitive)) {
            return mb_substr(haystack, 0, compare_func(haystack, needle, case_sensitive));
        }

        return haystack;
    }

    static replaceFirstOccurrence(search, replace, subject) {
        if (!search) {
            return subject;
        }

        var pos = mb_strpos(subject, search);

        if (pos !== false) {
            subject = this.mb_substr_replace(subject, replace, pos, mb_strlen(search));
        }

        return subject;
    }

    static replaceLastOccurrence(search, replace, subject) {
        var pos = strrpos(subject, search);

        if (pos !== false) {
            subject = this.mb_substr_replace(subject, replace, pos, mb_strlen(search));
        }

        return subject;
    }

    static mb_substr_replace(original, replacement, position, length) {
        var startString = mb_substr(original, 0, position, "UTF-8");
        var endString = mb_substr(original, position + length, mb_strlen(original), "UTF-8");
        return startString + replacement + endString;
    }

    static mergeObjects(obj1, obj2) {
        return Object(array_merge(Array.from(obj1 || ""), Array.from(obj2 || "")));
    }

    static arrayMergeRecursiveUnique(array1, array2) {
        if (!array1) //Optimize the base case
            {
                return array2;
            }

        for (var key in array2) {
            var value = array2[key];

            if (Array.isArray(value) && undefined !== array1[key] && Array.isArray(array1[key])) {
                var value = this.arrayMergeRecursiveUnique(array1[key], value);
            }

            array1[key] = value;
        }

        return array1;
    }

    static arrayDiffMultidimensional(array1, array2) {
        var result = Array();

        for (var key in array1) {
            var value = array1[key];

            if (!Array.isArray(array2) || !(key in array2)) {
                result[key] = value;
                continue;
            }

            if (Array.isArray(value)) {
                var recursiveArrayDiff = this.ArrayDiffMultidimensional(value, array2[key]);

                if (recursiveArrayDiff.length) {
                    result[key] = recursiveArrayDiff;
                }

                continue;
            }

            if (value != array2[key]) {
                result[key] = value;
            }
        }

        return result;
    }

    static arrayCombine(arr1, arr2) {
        var count = Math.min(count(arr1), count(arr2));
        return array_combine(arr1.slice(0, count), arr2.slice(0, count));
    }

    static arrayIsAssociative(arr) {
        if (Array() === arr) {
            return false;
        }

        return Object.keys(arr) !== Array(Math.ceil(arr.length - 1).fill(0));
    }

    static dateFormats() {
        var now = new DateTime();
        var date_formats = {
            "d-m-Y H:i": "d-m-Y H:i (e.g. " + now.format("d-m-Y H:i") + ")",
            "Y-m-d H:i": "Y-m-d H:i (e.g. " + now.format("Y-m-d H:i") + ")",
            "m/d/Y h:i a": "m/d/Y h:i a (e.g. " + now.format("m/d/Y h:i a") + ")",
            "H:i d-m-Y": "H:i d-m-Y (e.g. " + now.format("H:i d-m-Y") + ")",
            "h:i a m/d/Y": "h:i a m/d/Y (e.g. " + now.format("h:i a m/d/Y") + ")"
        };
        var default_format = Grav.instance().config.get("system.pages.dateformat.default");

        if (default_format) {
            date_formats = array_merge({
                [default_format]: default_format + " (e.g. " + now.format(default_format) + ")"
            }, date_formats);
        }

        return date_formats;
    }

    static dateNow(default_format = undefined) {
        var now = new DateTime();

        if (is_null(default_format)) {
            default_format = Grav.instance().config.get("system.pages.dateformat.default");
        }

        return now.format(default_format);
    }

    static truncate(string, limit = 150, up_to_break = false, break_r = " ", pad = "&hellip;") //return with no change if string is shorter than $limit
    {
        if (mb_strlen(string) <= limit) {
            return string;
        }

        if (up_to_break && false !== (breakpoint = mb_strpos(string, break_r, limit))) {
            if (breakpoint < mb_strlen(string) - 1) {
                string = mb_substr(string, 0, breakpoint) + pad;
            }
        } else {
            string = mb_substr(string, 0, limit) + pad;
        }

        return string;
    }

    static safeTruncate(string, limit = 150) {
        return this.truncate(string, limit, true);
    }

    static truncateHtml(text, length = 100, ellipsis = "...") {
        return Truncator.truncateLetters(text, length, ellipsis);
    }

    static safeTruncateHtml(text, length = 25, ellipsis = "...") {
        return Truncator.truncateWords(text, length, ellipsis);
    }

    static generateRandomString(length = 5) {
        return str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ").substr(0, length);
    }

    static download(file, force_download = true, sec = 0, bytes = 1024) {
        if (file_exists(file)) //fire download event
            //File size
            //clean all buffers
            //required for IE, otherwise Content-Disposition may be ignored
            //output the file itself
            //you may want to change this
            {
                Grav.instance().fireEvent("onBeforeDownload", new Event({
                    file: file
                }));
                var file_parts = pathinfo(file);
                var mimetype = this.getMimeByExtension(file_parts.extension);
                var size = filesize(file);

                while (ob_get_level()) {
                    ob_end_clean();
                }

                if (ini_get("zlib.output_compression")) {
                    ini_set("zlib.output_compression", "Off");
                }

                header("Content-Type: " + mimetype);
                header("Accept-Ranges: bytes");

                if (force_download) //output the regular HTTP headers
                    {
                        header("Content-Disposition: attachment; filename=\"" + file_parts.basename + "\"");
                    }

                if (undefined !== _SERVER.HTTP_RANGE) {
                    var a, range, range_end;
                    [a, range] = _SERVER.HTTP_RANGE.split("=", 2);
                    [range] = range.split(",", 2);
                    [range, range_end] = range.split("-");
                    range = +range;

                    if (!range_end) {
                        range_end = size - 1;
                    } else {
                        range_end = +range_end;
                    }

                    var new_length = range_end - range + 1;
                    header("HTTP/1.1 206 Partial Content");
                    header(`Content-Length: ${new_length}`);
                    header(`Content-Range: bytes ${range}-${range_end}/${size}`);
                } else {
                    range = 0;
                    new_length = size;
                    header("Content-Length: " + size);

                    if (Grav.instance().config.get("system.cache.enabled")) //Return 304 Not Modified if the file is already cached in the browser
                        {
                            var expires = Grav.instance().config.get("system.pages.expires");

                            if (expires > 0) {
                                var expires_date = gmdate("D, d M Y H:i:s T", Date.now() / 1000 + expires);
                                header("Cache-Control: max-age=" + expires);
                                header("Expires: " + expires_date);
                                header("Pragma: cache");
                            }

                            header("Last-Modified: " + gmdate("D, d M Y H:i:s T", filemtime(file)));

                            if (undefined !== _SERVER.HTTP_IF_MODIFIED_SINCE && strtotime(_SERVER.HTTP_IF_MODIFIED_SINCE) >= filemtime(file)) {
                                header("HTTP/1.1 304 Not Modified");
                                throw die();
                            }
                        }
                }

                var chunksize = bytes * 8;
                var bytes_send = 0;
                var fp = fopen(file, "rb");

                if (fp) {
                    if (range) {
                        fseek(fp, range);
                    }

                    while (!feof(fp) && !connection_aborted() && bytes_send < new_length) //echo($buffer); // is also possible
                    {
                        var buffer = fread(fp, chunksize);
                        echo(buffer);
                        flush();
                        usleep(sec * 1000000);
                        bytes_send += buffer.length;
                    }

                    fclose(fp);
                } else {
                    throw new RuntimeException("Error - can not open file.");
                }

                throw die();
            }
    }

    static getMimeByExtension(extension, default_r = "application/octet-stream") //look for some standard types
    {
        extension = extension.toLowerCase();

        switch (extension) {
            case undefined:
                return default_r;

            case "json":
                return "application/json";

            case "html":
                return "text/html";

            case "atom":
                return "application/atom+xml";

            case "rss":
                return "application/rss+xml";

            case "xml":
                return "application/xml";
        }

        var media_types = Grav.instance().config.get("media.types");

        if (undefined !== media_types[extension]) {
            if (undefined !== media_types[extension].mime) {
                return media_types[extension].mime;
            }
        }

        return default_r;
    }

    static getMimeTypes(extensions) {
        var mimetypes = Array();

        for (var extension of Object.values(extensions)) {
            var mimetype = this.getMimeByExtension(extension, false);

            if (mimetype && !(-1 !== mimetypes.indexOf(mimetype))) {
                mimetypes.push(mimetype);
            }
        }

        return mimetypes;
    }

    static getExtensionByMime(mime, default_r = "html") //look for some standard mime types
    {
        mime = mime.toLowerCase();

        switch (mime) {
            case "*/*":
            case "text/*":
            case "text/html":
                return "html";

            case "application/json":
                return "json";

            case "application/atom+xml":
                return "atom";

            case "application/rss+xml":
                return "rss";

            case "application/xml":
                return "xml";
        }

        var media_types = Array.from(Grav.instance().config.get("media.types") || "");

        for (var extension in media_types) {
            var type = media_types[extension];

            if (extension === "defaults") {
                continue;
            }

            if (undefined !== type.mime && type.mime === mime) {
                return extension;
            }
        }

        return default_r;
    }

    static getExtensions(mimetypes) {
        var extensions = Array();

        for (var mimetype of Object.values(mimetypes)) {
            var extension = this.getExtensionByMime(mimetype, false);

            if (extension && !(-1 !== extensions.indexOf(extension))) {
                extensions.push(extension);
            }
        }

        return extensions;
    }

    static getMimeByFilename(filename, default_r = "application/octet-stream") {
        return this.getMimeByExtension(pathinfo(filename, PATHINFO_EXTENSION), default_r);
    }

    static getMimeByLocalFile(filename, default_r = "application/octet-stream") //For local files we can detect type by the file content.
    {
        var type = false;

        if (!stream_is_local(filename) || !file_exists(filename)) {
            return false;
        }

        if (extension_loaded("fileinfo")) {
            var finfo = finfo_open(FILEINFO_SYMLINK | FILEINFO_MIME_TYPE);
            type = finfo_file(finfo, filename);
            finfo_close(finfo);
        } else //Fall back to use getimagesize() if it is available (not recommended, but better than nothing)
            {
                var info = getimagesize(filename);

                if (info) {
                    type = info.mime;
                }
            }

        return type ? type : this.getMimeByFilename(filename, default_r);
    }

    static checkFilename(filename) {
        var dangerous_extensions = Grav.instance().config.get("security.uploads_dangerous_extensions", Array());
        dangerous_extensions.forEach(val => {
            val = "." + val;
        });
        var extension = "." + pathinfo(filename, PATHINFO_EXTENSION);
        return !(!filename || strtr(filename, "\t\x0B\n\r\\0\\/", "_______") !== filename || filename.replace(/^[\. ]*|[\. ]*$/g, "") !== filename || this.contains(extension, dangerous_extensions));
    }

    static normalizePath(path) //Resolve any streams
    //@var UniformResourceLocator $locator
    {
        var locator = Grav.instance().locator;

        if (locator.isStream(path)) {
            path = locator.findResource(path);
        }

        var root = "";

        if (matches) {
            root = matches[1];
            path = matches[2];
        }

        if (Utils.startsWith(path, "/")) {
            root += "/";
            path = path.replace(/^[\/]*/, "");
        }

        if (Utils.contains(path, "..")) {
            var segments = path.replace(/^[\/]*|[\/]*$/g, "").split("/");
            var ret = Array();

            for (var segment of Object.values(segments)) {
                if (segment === "." || segment === "") {
                    continue;
                }

                if (segment === "..") {
                    ret.pop();
                } else {
                    ret.push(segment);
                }
            }

            path = ret.join("/");
        }

        var normalized = root + path;
        return normalized;
    }

    static isFunctionDisabled(function_r) {
        return -1 !== ini_get("disable_functions").split(",").indexOf(function_r);
    }

    static timezones() {
        var timezones = DateTimeZone.listIdentifiers(DateTimeZone.ALL);
        var offsets = Array();
        var testDate = new DateTime();

        for (var zone of Object.values(timezones)) {
            var tz = new DateTimeZone(zone);
            offsets[zone] = tz.getOffset(testDate);
        }

        asort(offsets);
        var timezone_list = Array();

        for (var timezone in offsets) {
            var offset = offsets[timezone];
            var offset_prefix = offset < 0 ? "-" : "+";
            var offset_formatted = gmdate("H:i", Math.abs(offset));
            var pretty_offset = `UTC${offset_prefix}${offset_formatted}`;
            timezone_list[timezone] = `(${pretty_offset}) ` + str_replace("_", " ", timezone);
        }

        return timezone_list;
    }

    static arrayFilterRecursive(source, fn) {
        var result = Array();

        for (var key in source) {
            var value = source[key];

            if (Array.isArray(value)) {
                result[key] = this.arrayFilterRecursive(value, fn);
                continue;
            }

            if (fn(key, value)) //KEEP
                {
                    result[key] = value;
                    continue;
                }
        }

        return result;
    }

    static arrayFlatten(array) {
        var flatten = Array();

        for (var key in array) {
            var inner = array[key];

            if (Array.isArray(inner)) {
                for (var inner_key in inner) {
                    var value = inner[inner_key];
                    flatten[inner_key] = value;
                }
            } else {
                flatten[key] = inner;
            }
        }

        return flatten;
    }

    static arrayFlattenDotNotation(array, prepend = "") {
        var results = Array();

        for (var key in array) {
            var value = array[key];

            if (Array.isArray(value)) {
                results = array_merge(results, this.arrayFlattenDotNotation(value, prepend + key + "."));
            } else {
                results[prepend + key] = value;
            }
        }

        return results;
    }

    static arrayUnflattenDotNotation(array, separator = ".") {
        var newArray = Array();

        for (var key in array) {
            var value = array[key];
            var dots = key.split(separator);

            if (dots.length > 1) {
                var last = newArray[dots[0]];

                for (var k in dots) {
                    var dot = dots[k];

                    if (k === 0) {
                        continue;
                    }

                    last = last[dot];
                }

                last = value;
            } else {
                newArray[key] = value;
            }
        }

        return newArray;
    }

    static pathPrefixedByLangCode(string) {
        if (string.length <= 3) {
            return false;
        }

        var languages_enabled = Grav.instance().config.get("system.languages.supported", Array());
        var parts = string.replace(/^[\/]*|[\/]*$/g, "").split("/");

        if (parts.length > 0 && -1 !== languages_enabled.indexOf(parts[0])) {
            return true;
        }

        return false;
    }

    static date2timestamp(date, format = undefined) //try to use DateTime and default format
    //fallback to strtotime() if DateTime approach failed
    {
        var config = Grav.instance().config;
        var dateformat = format ? format : config.get("system.pages.dateformat.default");

        if (dateformat) {
            var datetime = DateTime.createFromFormat(dateformat, date);
        } else {
            datetime = new DateTime(date);
        }

        if (datetime !== false) {
            return datetime.getTimestamp();
        }

        return strtotime(date);
    }

    static resolve(array, path, default_r = undefined) {
        user_error("Utils_resolve" + "::" + "Utils_resolve" + "() is deprecated since Grav 1.5, use ->getDotNotation() method instead", E_USER_DEPRECATED);
        return this.getDotNotation(array, path, default_r);
    }

    static isPositive(value) {
        return -1 !== [true, 1, "1", "yes", "on", "true"].indexOf(value);
    }

    static generateNonceString(action, previousTick = false) {
        var username = "";

        if (undefined !== Grav.instance().user) {
            var user = Grav.instance().user;
            username = user.username;
        }

        var token = session_id();
        var i = Utils.nonceTick();

        if (previousTick) {
            i--;
        }

        return i + "|" + action + "|" + username + "|" + token + "|" + Grav.instance().config.get("security.salt");
    }

    static nonceTick() {
        var secondsInHalfADay = 60 * 60 * 12;
        return +Math.ceil(Date.now() / 1000 / secondsInHalfADay);
    }

    static verifyNonce(nonce, action) //Safety check for multiple nonces
    {
        if (Array.isArray(nonce)) {
            nonce = nonce.shift();
        }

        if (nonce === Utils.getNonce(action)) {
            return true;
        }

        var previousTick = true;
        return nonce === Utils.getNonce(action, previousTick);
    }

    static isAdminPlugin() {
        if (undefined !== Grav.instance().admin) {
            return true;
        }

        return false;
    }

    static getDotNotation(array, key, default_r = undefined) {
        if (undefined === key) {
            return array;
        }

        if (undefined !== array[key]) {
            return array[key];
        }

        for (var segment of Object.values(key.split("."))) {
            if (!Array.isArray(array) || !(segment in array)) {
                return default_r;
            }

            array = array[segment];
        }

        return array;
    }

    static setDotNotation(array, key, value, merge = false) {
        if (undefined === key) {
            return array = value;
        }

        var keys = key.split(".");

        while (keys.length > 1) {
            key = keys.shift();

            if (!(undefined !== array[key]) || !Array.isArray(array[key])) {
                array[key] = Array();
            }

            array = array[key];
        }

        key = keys.shift();

        if (!merge || !(undefined !== array[key])) {
            array[key] = value;
        } else {
            array[key] = array_merge(array[key], value);
        }

        return array;
    }

    static isWindows() {
        return strncasecmp(PHP_OS, "WIN", 3) === 0;
    }

    static isApache() {
        return undefined !== _SERVER.SERVER_SOFTWARE && strpos(_SERVER.SERVER_SOFTWARE, "Apache") !== false;
    }

    static sortArrayByArray(array, orderArray) {
        var ordered = Array();

        for (var key of Object.values(orderArray)) {
            if (key in array) {
                ordered[key] = array[key];
                delete array[key];
            }
        }

        return ordered + array;
    }

    static sortArrayByKey(array, array_key, direction = SORT_DESC, sort_flags = SORT_REGULAR) {
        var output = Array();

        if (!Array.isArray(array) || !array) {
            return output;
        }

        for (var key in array) {
            var row = array[key];
            output[key] = row[array_key];
        }

        array_multisort(output, direction, sort_flags, array);
        return array;
    }

    static getPagePathFromToken(path, page = undefined) {
        var path_parts = pathinfo(path);
        var grav = Grav.instance();
        var basename = "";

        if (undefined !== path_parts.extension) {
            basename = "/" + path_parts.basename;
            path = path_parts.dirname.replace(/:*$/, "");
        }

        var regex = "/(@self|self@)|((?:@page|page@):(?:.*))|((?:@theme|theme@):(?:.*))/";
        preg_match(regex, path, matches);

        if (matches) {
            if (matches[1]) {
                if (undefined === page) {
                    throw new RuntimeException("Page not available for this self@ reference");
                }
            } else if (matches[2]) //page@
                {
                    var parts = path.split(":");
                    var route = parts[1];
                    page = grav.page.find(route);
                } else if (matches[3]) //theme@
                {
                    parts = path.split(":");
                    route = parts[1];
                    var theme = str_replace(ROOT_DIR, "", grav.locator.findResource("theme://"));
                    return theme + route + basename;
                }
        } else {
            return path + basename;
        }

        if (!page) {
            throw new RuntimeException("Page route not found: " + path);
        }

        path = str_replace(matches[0], page.relativePagePath().replace(/[\/]*$/, ""), path);
        return path + basename;
    }

    static getUploadLimit() {
        if (!("_static_Utils_getUploadLimit_max_size" in global)) _static_Utils_getUploadLimit_max_size = -1;

        if (_static_Utils_getUploadLimit_max_size < 0) {
            var post_max_size = this.parseSize(ini_get("post_max_size"));

            if (post_max_size > 0) {
                _static_Utils_getUploadLimit_max_size = post_max_size;
            } else {
                _static_Utils_getUploadLimit_max_size = 0;
            }

            var upload_max = this.parseSize(ini_get("upload_max_filesize"));

            if (upload_max > 0 && upload_max < _static_Utils_getUploadLimit_max_size) {
                _static_Utils_getUploadLimit_max_size = upload_max;
            }
        }

        return _static_Utils_getUploadLimit_max_size;
    }

    static convertSize(bytes, to, decimal_places = 1) {
        var formulas = {
            K: number_format(bytes / 1024, decimal_places),
            M: number_format(bytes / 1048576, decimal_places),
            G: number_format(bytes / 1073741824, decimal_places)
        };
        return formulas[to] || 0;
    }

    static prettySize(bytes, precision = 2) //Uncomment one of the following alternatives
    //$bytes /= (1 << (10 * $pow));
    {
        var units = ["B", "KB", "MB", "GB", "TB"];
        bytes = Math.max(bytes, 0);
        var pow = Math.floor((bytes ? log(bytes) : 0) / log(1024));
        pow = Math.min(pow, units.length - 1);
        bytes /= pow(1024, pow);
        return Math.round(bytes, precision) + " " + units[pow];
    }

    static parseSize(size) {
        var unit = size.replace(/[^bkmgtpezy]/gi, "");
        size = size.replace(/[^0-9\.]/g, "");

        if (unit) {
            size = size * Math.pow(1024, stripos("bkmgtpezy", unit[0]));
        }

        return +Math.abs(Math.round(size));
    }

    static multibyteParseUrl(url) {
        var enc_url = url.replace(/[^:\/@?&=#]+/gu, matches => {
            return urlencode(matches[0]);
        });
        var parts = parse_url(enc_url);

        if (parts === false) {
            throw new InvalidArgumentException("Malformed URL: " + url);
        }

        for (var name in parts) {
            var value = parts[name];
            parts[name] = urldecode(value);
        }

        return parts;
    }

    static processMarkdown(string, block = true) //Initialize the preferred variant of Parsedown
    {
        var page = Grav.instance().page || undefined;
        var defaults = Grav.instance().config.get("system.pages.markdown");

        if (defaults.extra) {
            var parsedown = new ParsedownExtra(page, defaults);
        } else {
            parsedown = new Parsedown(page, defaults);
        }

        if (block) {
            string = parsedown.text(string);
        } else {
            string = parsedown.line(string);
        }

        return string;
    }

    static getSubnet(ip, prefix = 64) //Maximum netmask length = same as packed address
    //Packed representation of netmask
    //Bitwise - Take all bits that are both 1 to generate subnet
    {
        if (!filter_var(ip, FILTER_VALIDATE_IP)) {
            throw new InvalidArgumentException("Invalid IP: " + ip);
        }

        ip = inet_pton(ip);
        var len = 8 * ip.length;
        if (prefix > len) prefix = len;
        var mask = str_repeat("f", prefix >> 2);

        switch (prefix & 3) {
            case 3:
                mask += "e";
                break;

            case 2:
                mask += "c";
                break;

            case 1:
                mask += "8";
                break;
        }

        mask = str_pad(mask, len >> 2, "0");
        mask = pack("H*", mask);
        var subnet = inet_ntop(ip & mask);
        return subnet;
    }

}
