//
//@package    Grav\Common\Twig
//
//@copyright  Copyright (C) 2015 - 2019 Trilby Media, LLC. All rights reserved.
//@license    MIT License; see LICENSE file for details.
//

//
//Parses a token and returns a node.
//
//@param Token $token A Twig_Token instance
//
//@return Node A Twig_Node instance
//
//
//Gets the tag name associated with this token parser.
//
//@return string The tag name
//
export class TwigTokenParserThrow extends AbstractTokenParser {
  parse(token) {
    var lineno = token.getLine();
    var stream = this.parser.getStream();
    var code = stream.expect(Token.NUMBER_TYPE).getValue();
    var message = this.parser.getExpressionParser().parseExpression();
    stream.expect(Token.BLOCK_END_TYPE);
    return new TwigNodeThrow(code, message, lineno, this.getTag());
  }

  getTag() {
    return "throw";
  }

}
