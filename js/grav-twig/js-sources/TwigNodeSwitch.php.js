//
//@package    Grav\Common\Twig
//
//@copyright  Copyright (C) 2015 - 2019 Trilby Media, LLC. All rights reserved.
//@license    MIT License; see LICENSE file for details.
//

//
//TwigNodeSwitch constructor.
//@param Node $value
//@param Node $cases
//@param Node|null $default
//@param int $lineno
//@param string|null $tag
//
//
//Compiles the node to PHP.
//
//@param Compiler $compiler A Twig_Compiler instance
//
export class TwigNodeSwitch extends Node {
    constructor(value, cases, default_r = undefined, lineno = 0, tag = undefined) {
        super({
            value: value,
            cases: cases,
            default: default_r
        }, Array(), lineno, tag);
    }

    compile(compiler) {
        compiler.addDebugInfo(this).write("switch (").subcompile(this.getNode("value")).raw(") {\n").indent();

        for (var case_r of Object.values(this.getNode("cases"))) {
            if (!case_r.hasNode("body")) {
                continue;
            }

            for (var value of Object.values(case_r.getNode("values"))) {
                compiler.write("case ").subcompile(value).raw(":\n");
            }

            compiler.write("{\n").indent().subcompile(case_r.getNode("body")).write("break;\n").outdent().write("}\n");
        }

        if (this.hasNode("default") && this.getNode("default") !== undefined) {
            compiler.write("default:\n").write("{\n").indent().subcompile(this.getNode("default")).outdent().write("}\n");
        }

        compiler.outdent().write("}\n");
    }

}
