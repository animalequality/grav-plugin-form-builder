//
//@package    Grav\Common\Twig
//
//@copyright  Copyright (C) 2015 - 2019 Trilby Media, LLC. All rights reserved.
//@license    MIT License; see LICENSE file for details.
//

//
//TwigNodeMarkdown constructor.
//@param Node $body
//@param int $lineno
//@param string $tag
//
//
//Compiles the node to PHP.
//
//@param Compiler $compiler A Twig_Compiler instance
//
export class TwigNodeMarkdown extends Node {
  constructor(body, lineno, tag = "markdown") {
    super({
      body: body
    }, Array(), lineno, tag);
  }

  compile(compiler) {
    compiler.addDebugInfo(this).write("ob_start();" + PHP_EOL).subcompile(this.getNode("body")).write("$content = ob_get_clean();" + PHP_EOL).write("preg_match(\"/^\\s*/\", $content, $matches);" + PHP_EOL).write("$lines = explode(\"\n\", $content);" + PHP_EOL).write("$content = preg_replace('/^' . $matches[0]. '/', \"\", $lines);" + PHP_EOL).write("$content = join(\"\n\", $content);" + PHP_EOL).write("echo $this->env->getExtension('Grav\\Common\\Twig\\TwigExtension')->markdownFunction($content);" + PHP_EOL);
  }

}
