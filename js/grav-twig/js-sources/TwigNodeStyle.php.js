//
//@package    Grav\Common\Twig
//
//@copyright  Copyright (C) 2015 - 2019 Trilby Media, LLC. All rights reserved.
//@license    MIT License; see LICENSE file for details.
//

//
//TwigNodeAssets constructor.
//@param Node|null $body
//@param AbstractExpression|null $file
//@param AbstractExpression|null $group
//@param AbstractExpression|null $priority
//@param AbstractExpression|null $attributes
//@param int $lineno
//@param string|null $tag
//
//
//Compiles the node to PHP.
//
//@param Compiler $compiler A Twig_Compiler instance
//@throws \LogicException
//
export class TwigNodeStyle extends Node {
    constructor(body = undefined, file = undefined, group = undefined, priority = undefined, attributes = undefined, lineno = 0, tag = undefined) {
        super({
            body: body,
            file: file,
            group: group,
            priority: priority,
            attributes: attributes
        }, Array(), lineno, tag);
        this.tagName = "style";
    }

    compile(compiler) {
        compiler.addDebugInfo(this);
        compiler.write("$assets = \\Grav\\Common\\Grav::instance()['assets'];\n");

        if (this.getNode("attributes") !== undefined) {
            compiler.write("$attributes = ").subcompile(this.getNode("attributes")).raw(";\n").write("if (!is_array($attributes)) {\n").indent().write(`throw new UnexpectedValueException('{% ${this.tagName} with x %}: x is not an array');\n`).outdent().write("}\n");
        } else {
            compiler.write("$attributes = [];" + "\n");
        }

        if (this.getNode("group") !== undefined) {
            compiler.write("$attributes['group'] = ").subcompile(this.getNode("group")).raw(";\n").write("if (!is_string($attributes['group'])) {\n").indent().write(`throw new UnexpectedValueException('{% ${this.tagName} in x %}: x is not a string');\n`).outdent().write("}\n");
        }

        if (this.getNode("priority") !== undefined) {
            compiler.write("$attributes['priority'] = (int)(").subcompile(this.getNode("priority")).raw(");\n");
        }

        if (this.getNode("file") !== undefined) {
            compiler.write("$assets->addCss(").subcompile(this.getNode("file")).raw(", $attributes);\n");
        } else {
            compiler.write("ob_start();\n").subcompile(this.getNode("body")).write("$content = ob_get_clean();" + "\n").write("$assets->addInlineCss($content, $attributes);\n");
        }
    }

}
