//
//@package    Grav\Common\Twig
//
//@copyright  Copyright (C) 2015 - 2019 Trilby Media, LLC. All rights reserved.
//@license    MIT License; see LICENSE file for details.
//

//@var Grav
//@var Debugger
//@var Config
//
//TwigExtension constructor.
//
//
//Register some standard globals
//
//@return array
//
//
//Return a list of all filters.
//
//@return array
//
//
//Return a list of all functions.
//
//@return array
//
//
//@return array
//
//
//Filters field name by changing dot notation into array notation.
//
//@param  string $str
//
//@return string
//
//
//Protects email address.
//
//@param  string $str
//
//@return string
//
//
//Returns array in a random order.
//
//@param  array $original
//@param  int   $offset Can be used to return only slice of the array.
//
//@return array
//
//
//Returns the modulus of an integer
//
//@param  string|int   $number
//@param  int          $divider
//@param  array        $items array of items to select from to return
//
//@return int
//
//
//Inflector supports following notations:
//
//`{{ 'person'|pluralize }} => people`
//`{{ 'shoes'|singularize }} => shoe`
//`{{ 'welcome page'|titleize }} => "Welcome Page"`
//`{{ 'send_email'|camelize }} => SendEmail`
//`{{ 'CamelCased'|underscorize }} => camel_cased`
//`{{ 'Something Text'|hyphenize }} => something-text`
//`{{ 'something_text_to_read'|humanize }} => "Something text to read"`
//`{{ '181'|monthize }} => 5`
//`{{ '10'|ordinalize }} => 10th`
//
//@param string $action
//@param string $data
//@param int    $count
//
//@return string
//
//
//Return MD5 hash from the input.
//
//@param  string $str
//
//@return string
//
//
//Return Base32 encoded string
//
//@param string $str
//@return string
//
//
//Return Base32 decoded string
//
//@param string $str
//@return bool|string
//
//
//Return Base64 encoded string
//
//@param string $str
//@return string
//
//
//Return Base64 decoded string
//
//@param string $str
//@return bool|string
//
//
//Sorts a collection by key
//
//@param  array    $input
//@param  string   $filter
//@param  int      $direction
//@param  int      $sort_flags
//
//@return array
//
//
//Return ksorted collection.
//
//@param  array $array
//
//@return array
//
//
//Wrapper for chunk_split() function
//
//@param string $value
//@param int $chars
//@param string $split
//@return string
//
//
//determine if a string contains another
//
//@param string $haystack
//@param string $needle
//
//@return bool
//
//
//Gets a human readable output for cron sytnax
//
//@param $at
//@return string
//
//
//Get Cron object for a crontab 'at' format
//
//@param string $at
//@return CronExpression
//
//
//displays a facebook style 'time ago' formatted date/time
//
//@param string $date
//@param bool $long_strings
//
//@param bool $show_tense
//@return bool
//
//
//Allow quick check of a string for XSS Vulnerabilities
//
//@param string|array $data
//@return bool|string|array
//
//
//@param string $string
//
//@return mixed
//
//
//@param string $string
//
//@param bool $block  Block or Line processing
//@return mixed|string
//
//
//@param string $haystack
//@param string $needle
//
//@return bool
//
//
//@param string $haystack
//@param string $needle
//
//@return bool
//
//
//@param mixed $value
//@param null $default
//
//@return null
//
//
//@param string $value
//@param null $chars
//
//@return string
//
//
//@param string $value
//@param null $chars
//
//@return string
//
//
//Casts input to string.
//
//@param mixed $input
//@return string
//
//
//Casts input to int.
//
//@param mixed $input
//@return int
//
//
//Casts input to bool.
//
//@param mixed $input
//@return bool
//
//
//Casts input to float.
//
//@param mixed $input
//@return float
//
//
//Casts input to array.
//
//@param mixed $input
//@return array
//
//
//@return string
//
//
//Translate Strings
//
//@param string|array $args
//@param array|null $languages
//@param bool $array_support
//@param bool $html_out
//@return string
//
//
//@param string $key
//@param string $index
//@param array|null $lang
//@return string
//
//
//Repeat given string x times.
//
//@param  string $input
//@param  int    $multiplier
//
//@return string
//
//
//Return URL to the resource.
//
//@example {{ url('theme://images/logo.png')|default('http://www.placehold.it/150x100/f4f4f4') }}
//
//@param  string $input  Resource to be located.
//@param  bool   $domain True to include domain name.
//
//@return string|null      Returns url to the resource or null if resource was not found.
//
//
//This function will evaluate Twig $twig through the $environment, and return its results.
//
//@param array $context
//@param string $twig
//@return mixed
//
//
//This function will evaluate a $string through the $environment, and return its results.
//
//@param array $context
//@param string $string
//@return mixed
//
//
//Based on Twig_Extension_Debug / twig_var_dump
//(c) 2011 Fabien Potencier
//
//@param \Twig_Environment $env
//@param string $context
//
//
//Output a Gist
//
//@param  string $id
//@param  string|bool $file
//
//@return string
//
//
//Generate a random string
//
//@param int $count
//
//@return string
//
//
//Pad a string to a certain length with another string
//
//@param string $input
//@param int    $pad_length
//@param string $pad_string
//@param int    $pad_type
//
//@return string
//
//
//Workaround for twig associative array initialization
//Returns a key => val array
//
//@param string $key           key of item
//@param string $val           value of item
//@param array  $current_array optional array to add to
//
//@return array
//
//
//Wrapper for array_intersect() method
//
//@param array $array1
//@param array $array2
//@return array
//
//
//Returns a string from a value. If the value is array, return it json encoded
//
//@param array|string $value
//
//@return string
//
//
//Translate a string
//
//@return string
//
//
//Authorize an action. Returns true if the user is logged in and
//has the right to execute $action.
//
//@param  string|array $action An action or a list of actions. Each
//entry can be a string like 'group.action'
//or without dot notation an associative
//array.
//@return bool                 Returns TRUE if the user is authorized to
//perform the action, FALSE otherwise.
//
//
//Used to add a nonce to a form. Call {{ nonce_field('action') }} specifying a string representing the action.
//
//For maximum protection, ensure that the string representing the action is as specific as possible
//
//@param string $action         the action
//@param string $nonceParamName a custom nonce param name
//
//@return string the nonce input field
//
//
//Decodes string from JSON.
//
//@param  string  $str
//@param  bool  $assoc
//@param  int $depth
//@param  int $options
//@return array
//
//
//Used to retrieve a cookie value
//
//@param string $key     The cookie name to retrieve
//
//@return mixed
//
//
//Twig wrapper for PHP's preg_replace method
//
//@param mixed $subject the content to perform the replacement on
//@param mixed $pattern the regex pattern to use for matches
//@param mixed $replace the replacement value either as a string or an array of replacements
//@param int   $limit   the maximum possible replacements for each pattern in each subject
//
//@return string|string[]|null the resulting content
//
//
//Twig wrapper for PHP's preg_grep method
//
//@param array $array
//@param string $regex
//@param int $flags
//@return array
//
//
//redirect browser from twig
//
//@param string $url          the url to redirect to
//@param int $statusCode      statusCode, default 303
//
//
//Generates an array containing a range of elements, optionally stepped
//
//@param int $start      Minimum number, default 0
//@param int $end        Maximum number, default `getrandmax()`
//@param int $step       Increment between elements in the sequence, default 1
//
//@return array
//
//
//Check if HTTP_X_REQUESTED_WITH has been set to xmlhttprequest,
//in which case we may unsafely assume ajax. Non critical use only.
//
//@return bool True if HTTP_X_REQUESTED_WITH exists and has been set to xmlhttprequest
//
//
//Get's the Exif data for a file
//
//@param string $image
//@param bool $raw
//@return mixed
//
//
//Simple function to read a file based on a filepath and output it
//
//@param string $filepath
//@return bool|string
//
//
//Process a folder as Media and return a media object
//
//@param string $media_dir
//@return Media|null
//
//
//Dump a variable to the browser
//
//@param mixed $var
//
//
//Returns a nicer more readable filesize based on bytes
//
//@param int $bytes
//@return string
//
//
//Returns a nicer more readable number
//
//@param int|float|string $n
//@return string|bool
//
//
//Get a theme variable
//
//@param string $var
//@param bool $default
//@return string
//
//
//takes an array of classes, and if they are not set on body_classes
//look to see if they are set in theme config
//
//@param string|string[] $classes
//@return string
//
//
//Look for a page header variable in an array of pages working its way through until a value is found
//
//@param string $var
//@param string|string[]|null $pages
//@return mixed
//
//
//Dump/Encode data into YAML format
//
//@param array $data
//@param int $inline integer number of levels of inline syntax
//@return string
//
//
//Decode/Parse data from YAML format
//
//@param string $data
//@return array
//
//
//Function/Filter to return the type of variable
//
//@param mixed $var
//@return string
//
//
//Function/Filter to test type of variable
//
//@param mixed $var
//@param string|null $typeTest
//@param string|null $className
//@return bool
//
export default class TwigExtension extends Twig_Extension {
    constructor() {
super()
        this.grav = Grav.instance();
        this.debugger = this.grav.debugger || undefined;
        this.config = this.grav.config;
    }

    getGlobals() {
        return {
            grav: this.grav
        };
    }

    getFilters() {
        return [new Twig_SimpleFilter("*ize", [this, "inflectorFilter"]), new Twig_SimpleFilter("absolute_url", [this, "absoluteUrlFilter"]), new Twig_SimpleFilter("contains", [this, "containsFilter"]), new Twig_SimpleFilter("chunk_split", [this, "chunkSplitFilter"]), new Twig_SimpleFilter("nicenumber", [this, "niceNumberFunc"]), new Twig_SimpleFilter("nicefilesize", [this, "niceFilesizeFunc"]), new Twig_SimpleFilter("nicetime", [this, "nicetimeFunc"]), new Twig_SimpleFilter("defined", [this, "definedDefaultFilter"]), new Twig_SimpleFilter("ends_with", [this, "endsWithFilter"]), new Twig_SimpleFilter("fieldName", [this, "fieldNameFilter"]), new Twig_SimpleFilter("ksort", [this, "ksortFilter"]), new Twig_SimpleFilter("ltrim", [this, "ltrimFilter"]), new Twig_SimpleFilter("markdown", [this, "markdownFunction"], {
            is_safe: ["html"]
        }), new Twig_SimpleFilter("md5", [this, "md5Filter"]), new Twig_SimpleFilter("base32_encode", [this, "base32EncodeFilter"]), new Twig_SimpleFilter("base32_decode", [this, "base32DecodeFilter"]), new Twig_SimpleFilter("base64_encode", [this, "base64EncodeFilter"]), new Twig_SimpleFilter("base64_decode", [this, "base64DecodeFilter"]), new Twig_SimpleFilter("randomize", [this, "randomizeFilter"]), new Twig_SimpleFilter("modulus", [this, "modulusFilter"]), new Twig_SimpleFilter("rtrim", [this, "rtrimFilter"]), new Twig_SimpleFilter("pad", [this, "padFilter"]), new Twig_SimpleFilter("regex_replace", [this, "regexReplace"]), new Twig_SimpleFilter("safe_email", [this, "safeEmailFilter"]), new Twig_SimpleFilter("safe_truncate", ["\\Grav\\Common\\Utils", "safeTruncate"]), new Twig_SimpleFilter("safe_truncate_html", ["\\Grav\\Common\\Utils", "safeTruncateHTML"]), new Twig_SimpleFilter("sort_by_key", [this, "sortByKeyFilter"]), new Twig_SimpleFilter("starts_with", [this, "startsWithFilter"]), new Twig_SimpleFilter("truncate", ["\\Grav\\Common\\Utils", "truncate"]), new Twig_SimpleFilter("truncate_html", ["\\Grav\\Common\\Utils", "truncateHTML"]), new Twig_SimpleFilter("json_decode", [this, "jsonDecodeFilter"]), new Twig_SimpleFilter("array_unique", "array_unique"), new Twig_SimpleFilter("basename", "basename"), new Twig_SimpleFilter("dirname", "dirname"), new Twig_SimpleFilter("print_r", "print_r"), new Twig_SimpleFilter("yaml_encode", [this, "yamlEncodeFilter"]), new Twig_SimpleFilter("yaml_decode", [this, "yamlDecodeFilter"]), new Twig_SimpleFilter("nicecron", [this, "niceCronFilter"]), new Twig_SimpleFilter("t", [this, "translate"], {
            needs_environment: true
        }), new Twig_SimpleFilter("tl", [this, "translateLanguage"]), new Twig_SimpleFilter("ta", [this, "translateArray"]), new Twig_SimpleFilter("string", [this, "stringFilter"]), new Twig_SimpleFilter("int", [this, "intFilter"], {
            is_safe: ["all"]
        }), new Twig_SimpleFilter("bool", [this, "boolFilter"]), new Twig_SimpleFilter("float", [this, "floatFilter"], {
            is_safe: ["all"]
        }), new Twig_SimpleFilter("array", [this, "arrayFilter"]), new Twig_SimpleFilter("get_type", [this, "getTypeFunc"]), new Twig_SimpleFilter("of_type", [this, "ofTypeFunc"])];
    }

    getFunctions() {
        return [new Twig_SimpleFunction("array", [this, "arrayFilter"]), new Twig_SimpleFunction("array_key_value", [this, "arrayKeyValueFunc"]), new Twig_SimpleFunction("array_key_exists", "array_key_exists"), new Twig_SimpleFunction("array_unique", "array_unique"), new Twig_SimpleFunction("array_intersect", [this, "arrayIntersectFunc"]), new Twig_SimpleFunction("authorize", [this, "authorize"]), new Twig_SimpleFunction("debug", [this, "dump"], {
            needs_context: true,
            needs_environment: true
        }), new Twig_SimpleFunction("dump__", [this, "dump"], {
            needs_context: true,
            needs_environment: true
        }), new Twig_SimpleFunction("vardump", [this, "vardumpFunc"]), new Twig_SimpleFunction("print_r", "print_r"), new Twig_SimpleFunction("http_response_code", "http_response_code"), new Twig_SimpleFunction("evaluate", [this, "evaluateStringFunc"], {
            needs_context: true
        }), new Twig_SimpleFunction("evaluate_twig", [this, "evaluateTwigFunc"], {
            needs_context: true
        }), new Twig_SimpleFunction("gist", [this, "gistFunc"]), new Twig_SimpleFunction("nonce_field", [this, "nonceFieldFunc"]), new Twig_SimpleFunction("pathinfo", "pathinfo"), new Twig_SimpleFunction("random_string", [this, "randomStringFunc"]), new Twig_SimpleFunction("repeat", [this, "repeatFunc"]), new Twig_SimpleFunction("regex_replace", [this, "regexReplace"]), new Twig_SimpleFunction("regex_filter", [this, "regexFilter"]), new Twig_SimpleFunction("string", [this, "stringFunc"]), new Twig_SimpleFunction("url", [this, "urlFunc"]), new Twig_SimpleFunction("json_decode", [this, "jsonDecodeFilter"]), new Twig_SimpleFunction("get_cookie", [this, "getCookie"]), new Twig_SimpleFunction("redirect_me", [this, "redirectFunc"]), new Twig_SimpleFunction("range__", [this, "rangeFunc"]), new Twig_SimpleFunction("isajaxrequest", [this, "isAjaxFunc"]), new Twig_SimpleFunction("exif", [this, "exifFunc"]), new Twig_SimpleFunction("media_directory", [this, "mediaDirFunc"]), new Twig_SimpleFunction("body_class", [this, "bodyClassFunc"]), new Twig_SimpleFunction("theme_var", [this, "themeVarFunc"]), new Twig_SimpleFunction("header_var", [this, "pageHeaderVarFunc"]), new Twig_SimpleFunction("read_file", [this, "readFileFunc"]), new Twig_SimpleFunction("nicenumber", [this, "niceNumberFunc"]), new Twig_SimpleFunction("nicefilesize", [this, "niceFilesizeFunc"]), new Twig_SimpleFunction("nicetime", [this, "nicetimeFunc"]), new Twig_SimpleFunction("cron", [this, "cronFunc"]), new Twig_SimpleFunction("xss", [this, "xssFunc"]), new Twig_SimpleFunction("t", [this, "translate"], {
            needs_environment: true
        }), new Twig_SimpleFunction("tl", [this, "translateLanguage"]), new Twig_SimpleFunction("ta", [this, "translateArray"]), new Twig_SimpleFunction("get_type", [this, "getTypeFunc"]), new Twig_SimpleFunction("of_type", [this, "ofTypeFunc"])];
    }

    getTokenParsers() {
        return [new TwigTokenParserRender(), new TwigTokenParserThrow(), new TwigTokenParserTryCatch(), new TwigTokenParserScript(), new TwigTokenParserStyle(), new TwigTokenParserMarkdown(), new TwigTokenParserSwitch()];
    }

    fieldNameFilter(str) {
        var path = str.replace(/[\.]*$/, "").split(".");
        return path.shift() + (path ? "[" + path.join("][") + "]" : "");
    }

    safeEmailFilter(str) {
        var i, len;
        var email = "";

        for (i = 0, len = str.length; i < len; i++) {
            var j = random_int(0, 1);
            email += j === 0 ? "&#" + str.charCodeAt(i) + ";" : str[i];
        }

        return str_replace("@", "&#64;", email);
    }

    randomizeFilter(original, offset = 0) {
        if (!Array.isArray(original)) {
            return original;
        }

        if (original instanceof Traversable) {
            original = iterator_to_array(original, false);
        }

        var sorted = Array();
        var random = original.slice(offset);
        shuffle(random);
        var sizeOf = original.length;

        for (var x = 0; x < sizeOf; x++) {
            if (x < offset) {
                sorted.push(original[x]);
            } else {
                sorted.push(random.shift());
            }
        }

        return sorted;
    }

    modulusFilter(number, divider, items = undefined) {
        if ("string" === typeof number) {
            number = number.length;
        }

        var remainder = number % divider;

        if (Array.isArray(items)) {
            return items[remainder] || items[0];
        }

        return remainder;
    }

    inflectorFilter(action, data, count = undefined) {
        action += "ize";
        var inflector = this.grav.inflector;

        if (-1 !== ["titleize", "camelize", "underscorize", "hyphenize", "humanize", "ordinalize", "monthize"].indexOf(action)) {
            return inflector[action](data);
        }

        if (-1 !== ["pluralize", "singularize"].indexOf(action)) {
            return count ? inflector[action](data, count) : inflector[action](data);
        }

        return data;
    }

    md5Filter(str) {
        return md5(str);
    }

    base32EncodeFilter(str) {
        return Base32.encode(str);
    }

    base32DecodeFilter(str) {
        return Base32.decode(str);
    }

    base64EncodeFilter(str) {
        return base64_encode(str);
    }

    base64DecodeFilter(str) {
        return base64_decode(str);
    }

    sortByKeyFilter(input, filter, direction = SORT_ASC, sort_flags = SORT_REGULAR) {
        return Utils.sortArrayByKey(input, filter, direction, sort_flags);
    }

    ksortFilter(array) {
        if (undefined === array) {
            array = Array();
        }

        ksort(array);
        return array;
    }

    chunkSplitFilter(value, chars, split = "-") {
        return chunk_split(value, chars, split);
    }

    containsFilter(haystack, needle) {
        if (!needle) {
            return haystack;
        }

        return strpos(haystack, String(needle)) !== false;
    }

    niceCronFilter(at) {
        var cron = new Cron(at);
        return cron.getText("en");
    }

    cronFunc(at) {
        return CronExpression.factory(at);
    }

    nicetimeFunc(date, long_strings = true, show_tense = true) //check if unix timestamp
    //check validity of date
    {
        if (!date) {
            return this.grav.language.translate("GRAV.NICETIME.NO_DATE_PROVIDED", undefined, true);
        }

        if (long_strings) {
            var periods = ["NICETIME.SECOND", "NICETIME.MINUTE", "NICETIME.HOUR", "NICETIME.DAY", "NICETIME.WEEK", "NICETIME.MONTH", "NICETIME.YEAR", "NICETIME.DECADE"];
        } else {
            periods = ["NICETIME.SEC", "NICETIME.MIN", "NICETIME.HR", "NICETIME.DAY", "NICETIME.WK", "NICETIME.MO", "NICETIME.YR", "NICETIME.DEC"];
        }

        var lengths = ["60", "60", "24", "7", "4.35", "12", "10"];
        var now = time();

        if (String(+(date === String(date)))) {
            var unix_date = date;
        } else {
            unix_date = strtotime(date);
        }

        if (!unix_date) {
            return this.grav.language.translate("GRAV.NICETIME.BAD_DATE", undefined, true);
        }

        if (now > unix_date) {
            var difference = now - unix_date;
            var tense = this.grav.language.translate("GRAV.NICETIME.AGO", undefined, true);
        } else if (now == unix_date) {
            difference = now - unix_date;
            tense = this.grav.language.translate("GRAV.NICETIME.JUST_NOW", undefined, false);
        } else {
            difference = unix_date - now;
            tense = this.grav.language.translate("GRAV.NICETIME.FROM_NOW", undefined, true);
        }

        for (var j = 0; difference >= lengths[j] && j < lengths.length - 1; j++) {
            difference /= lengths[j];
        }

        difference = Math.round(difference);

        if (difference != 1) {
            periods[j] += "_PLURAL";
        }

        if (this.grav.language.getTranslation(this.grav.language.getLanguage(), periods[j] + "_MORE_THAN_TWO")) {
            if (difference > 2) {
                periods[j] += "_MORE_THAN_TWO";
            }
        }

        periods[j] = this.grav.language.translate("GRAV." + periods[j], undefined, true);

        if (now == unix_date) {
            return tense;
        }

        var time = `${difference} ${periods[j]}`;
        time += show_tense ? ` ${tense}` : "";
        return time;
    }

    xssFunc(data) {
        if (!Array.isArray(data)) {
            return Security.detectXss(data);
        }

        var results = Security.detectXssFromArray(data);
        var results_parts = Object.values(results).map((value, key) => {
            return key + ": '" + value + "'";
        });
        return results_parts.join(", ");
    }

    absoluteUrlFilter(string) {
        var url = this.grav.uri.base();
        string = string.replace(/((?:href|src) *= *['"](?!(http|ftp)))/gi, `$1${url}`);
        return string;
    }

    markdownFunction(string, block = true) {
        return Utils.processMarkdown(string, block);
    }

    startsWithFilter(haystack, needle) {
        return Utils.startsWith(haystack, needle);
    }

    endsWithFilter(haystack, needle) {
        return Utils.endsWith(haystack, needle);
    }

    definedDefaultFilter(value, default_r = undefined) {
        return undefined !== value ? value : default_r;
    }

    rtrimFilter(value, chars = undefined) {
        return value.replace(new Regexp('[' + chars + ']*$'), "");
    }

    ltrimFilter(value, chars = undefined) {
        return value.replace(new Regexp('^[' + chars + ']*'), "");
    }

    stringFilter(input) {
        return String(input);
    }

    intFilter(input) {
        return +input;
    }

    boolFilter(input) {
        return !!input;
    }

    floatFilter(input) {
        return +input;
    }

    arrayFilter(input) {
        return Array.from(input || "");
    }

    translate(twig) //shift off the environment
    //If admin and tu filter provided, use it
    {
        var args = Array.from(arguments);
        args.shift();

        if (undefined !== this.grav.admin) {
            var numargs = args.length;
            var lang = undefined;

            if (numargs === 3 && Array.isArray(args[1]) || numargs === 2 && !Array.isArray(args[1])) {
                lang = args.pop();
            } else if (numargs === 2 && Array.isArray(args[1])) {
                var subs = args.pop();
                args = array_merge(args, subs);
            }

            return this.grav.admin.translate(args, lang);
        }

        return this.grav.language.translate(args);
    }

    translateLanguage(args, languages = undefined, array_support = false, html_out = false) //@var Language $language
    {
        var language = this.grav.language;
        return language.translate(args, languages, array_support, html_out);
    }

    translateArray(key, index, lang = undefined) //@var Language $language
    {
        var language = this.grav.language;
        return language.translateArray(key, index, lang);
    }

    repeatFunc(input, multiplier) {
        return str_repeat(input, multiplier);
    }

    urlFunc(input, domain = false) {
        return Utils.url(input, domain);
    }

    evaluateTwigFunc(context, twig) {
        var loader = new Twig_Loader_Filesystem(".");
        var env = new Twig_Environment(loader);
        var template = env.createTemplate(twig);
        return template.render(context);
    }

    evaluateStringFunc(context, string) {
        return this.evaluateTwigFunc(context, `{{ ${string} }}`);
    }

    dump(env, context) {
        if (!env.isDebug() || !this.debugger) {
            return;
        }

        var count = func_num_args();

        if (2 === count) {
            var data = Array();

            for (var key in context) {
                var value = context[key];

                if ("object" === typeof value) {
                    if (method_exists(value, "toArray")) {
                        data[key] = value.toArray();
                    } else {
                        data[key] = "Object (" + value.constructor.name + ")";
                    }
                } else {
                    data[key] = value;
                }
            }

            this.debugger.addMessage(data, "debug");
        } else {
            for (var i = 2; i < count; i++) {
                this.debugger.addMessage(func_get_arg(i), "debug");
            }
        }
    }

    gistFunc(id, file = false) {
        var url = "https://gist.github.com/" + id + ".js";

        if (file) {
            url += "?file=" + file;
        }

        return "<script src=\"" + url + "\"></script>";
    }

    randomStringFunc(count = 5) {
        return Utils.generateRandomString(count);
    }

    static padFilter(input, pad_length, pad_string = " ", pad_type = STR_PAD_RIGHT) {
        return str_pad(input, +pad_length, pad_string, pad_type);
    }

    arrayKeyValueFunc(key, val, current_array = undefined) {
        if (!current_array) {
            return {
                [key]: val
            };
        }

        current_array[key] = val;
        return current_array;
    }

    arrayIntersectFunc(array1, array2) {
        if (array1 instanceof Collection && array2 instanceof Collection) {
            return array1.intersect(array2);
        }

        return array_intersect(array1, array2);
    }

    stringFunc(value) {
        if (Array.isArray(value)) //format the array as a string
            {
                return JSON.stringify(value);
            }

        return value;
    }

    translateFunc() {
        return this.grav.language.translate(arguments);
    }

    authorize(action) //@var UserInterface $user
    {
        var user = this.grav.user;

        if (!user.authenticated || undefined !== user.authorized && !user.authorized) {
            return false;
        }

        action = Array.from(action || "");

        for (var key in action) {
            var perms = action[key];
            var prefix = "number" === typeof key ? "" : key + ".";
            var perms = prefix ? Array.from(perms || "") : {
                [perms]: true
            };

            for (var action2 in perms) {
                var authenticated = perms[action2];

                if (user.authorize(prefix + action2)) {
                    return authenticated;
                }
            }
        }

        return false;
    }

    nonceFieldFunc(action, nonceParamName = "nonce") {
        var string = "<input type=\"hidden\" name=\"" + nonceParamName + "\" value=\"" + Utils.getNonce(action) + "\" />";
        return string;
    }

    jsonDecodeFilter(str, assoc = false, depth = 512, options = 0) {
        return json_decode(html_entity_decode(str), assoc, depth, options);
    }

    getCookie(key) {
        return filter_input(INPUT_COOKIE, key, FILTER_SANITIZE_STRING);
    }

    regexReplace(subject, pattern, replace, limit = -1) {
        return preg_replace(pattern, replace, subject, limit);
    }

    regexFilter(array, regex, flags = 0) {
        return preg_grep(regex, array, flags);
    }

    redirectFunc(url, statusCode = 303) {
        header("Location: " + url, true, statusCode);
        throw die();
    }

    rangeFunc(start = 0, end = 100, step = 1) {
        return Array(Math.ceil((end - start) / step)).fill(start).map((x, y) => x + y * step);
    }

    isAjaxFunc() {
        return !!_SERVER.HTTP_X_REQUESTED_WITH && _SERVER.HTTP_X_REQUESTED_WITH.toLowerCase() === "xmlhttprequest";
    }

    exifFunc(image, raw = false) {
        if (undefined !== this.grav.exif) //@var UniformResourceLocator $locator
            {
                var locator = this.grav.locator;

                if (locator.isStream(image)) {
                    image = locator.findResource(image);
                }

                var exif_reader = this.grav.exif.getReader();

                if (image & file_exists(image) && this.config.get("system.media.auto_metadata_exif") && exif_reader) {
                    var exif_data = exif_reader.read(image);

                    if (exif_data) {
                        if (raw) {
                            return exif_data.getRawData();
                        }

                        return exif_data.getData();
                    }
                }
            }

        return undefined;
    }

    readFileFunc(filepath) //@var UniformResourceLocator $locator
    {
        var locator = this.grav.locator;

        if (locator.isStream(filepath)) {
            filepath = locator.findResource(filepath);
        }

        if (filepath && file_exists(filepath)) {
            return file_get_contents(filepath);
        }

        return false;
    }

    mediaDirFunc(media_dir) //@var UniformResourceLocator $locator
    {
        var locator = this.grav.locator;

        if (locator.isStream(media_dir)) {
            media_dir = locator.findResource(media_dir);
        }

        if (media_dir && file_exists(media_dir)) {
            return new Media(media_dir);
        }

        return undefined;
    }

    vardumpFunc(var_r) {
        console.log(var_r);
    }

    niceFilesizeFunc(bytes) {
        return Utils.prettySize(bytes);
    }

    niceNumberFunc(n) {
        if (!("number" === typeof n) && !("number" === typeof n)) {
            if (!("string" === typeof n) || n === "") {
                return false;
            }

            var list = preg_split("/\\D+/", str_replace(",", "", n)).filter();
            n = reset(list);

            if (!is_numeric(n)) {
                return false;
            }

            n = +n;
        }

        if (n > 1000000000000) {
            return Math.round(n / 1000000000000, 2) + " t";
        }

        if (n > 1000000000) {
            return Math.round(n / 1000000000, 2) + " b";
        }

        if (n > 1000000) {
            return Math.round(n / 1000000, 2) + " m";
        }

        if (n > 1000) {
            return Math.round(n / 1000, 2) + " k";
        }

        return number_format(n);
    }

    themeVarFunc(var_r, default_r = undefined) {
        var header = this.grav.page.header();
        var header_classes = header[var_r] || undefined;
        return header_classes ? header_classes : this.config.get("theme." + var_r, default_r);
    }

    bodyClassFunc(classes) {
        var header = this.grav.page.header();
        var body_classes = header.body_classes || "";

        for (var class_r of Object.values(Array.from(classes || ""))) {
            if (!!body_classes && Utils.contains(body_classes, class_r)) {
                continue;
            }

            var val = this.config.get("theme." + class_r, false) ? class_r : false;
            body_classes += val ? " " + val : "";
        }

        return body_classes;
    }

    pageHeaderVarFunc(var_r, pages = undefined) {
        if (pages === undefined) {
            pages = this.grav.page;
        }

        if (!Array.isArray(pages)) {
            pages = [pages];
        }

        for (var page of Object.values(pages)) {
            if ("string" === typeof page) {
                var page = this.grav.pages.find(page);
            }

            if (page) {
                var header = page.header();

                if (undefined !== header[var_r]) {
                    return header[var_r];
                }
            }
        }

        return undefined;
    }

    yamlEncodeFilter(data, inline = 10) {
        return Yaml.dump(data, inline);
    }

    yamlDecodeFilter(data) {
        return Yaml.parse(data);
    }

    getTypeFunc(var_r) {
        return typeof var_r;
    }

    ofTypeFunc(var_r, typeTest = undefined, className = undefined) {
        switch (typeTest) {
            default:
                return false;
                break;

            case "array":
                return Array.isArray(var_r);
                break;

            case "bool":
                return "boolean" === typeof var_r;
                break;

            case "class":
                return "object" === typeof var_r === true && var_r.constructor.name === className;
                break;

            case "float":
                return "number" === typeof var_r;
                break;

            case "int":
                return "number" === typeof var_r;
                break;

            case "numeric":
                return is_numeric(var_r);
                break;

            case "object":
                return "object" === typeof var_r;
                break;

            case "scalar":
                return is_scalar(var_r);
                break;

            case "string":
                return "string" === typeof var_r;
                break;
        }
    }

}
