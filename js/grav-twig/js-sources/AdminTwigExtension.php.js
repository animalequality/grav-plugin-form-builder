//
//@var Grav
//
//
//@var Language $lang
//
export default class AdminTwigExtension extends Twig_Extension {
    constructor() {
super()
        this.grav = Grav.instance();
        this.lang = this.grav.user.language;
    }

    getFilters() {
        return [new Twig_SimpleFilter("tu", [this, "tuFilter"]), new Twig_SimpleFilter("toYaml", [this, "toYamlFilter"]), new Twig_SimpleFilter("fromYaml", [this, "fromYamlFilter"]), new Twig_SimpleFilter("adminNicetime", [this, "adminNicetimeFilter"]), new Twig_SimpleFilter("nested", [this, "nestedFilter"])];
    }

    getFunctions() {
        return [new Twig_SimpleFunction("getPageUrl", [this, "getPageUrl"], {
            needs_context: true
        }), new Twig_SimpleFunction("clone", [this, "cloneFunc"])];
    }

    nestedFilter(current, name) {
        var path = name.replace(/^[\.]*|[\.]*$/g, "").split(".");

        for (var field of Object.values(path)) {
            if ("object" === typeof current && undefined !== current[field]) {
                current = current[field];
            } else if (Array.isArray(current) && undefined !== current[field]) {
                current = current[field];
            } else {
                return undefined;
            }
        }

        return current;
    }

    cloneFunc(obj) {
        return clone(obj);
    }

    getPageUrl(context, page) {
        var page_route = page.rawRoute().replace(/^[\/]*|[\/]*$/g, "");
        var page_lang = page.language();
        var base_url = context.base_url;
        var base_url_simple = context.base_url_simple;
        var admin_lang = Grav.instance().session.admin_lang ? Grav.instance().session.admin_lang : "en";

        if (page_lang && page_lang !== admin_lang) {
            var page_url = base_url_simple + "/" + page_lang + "/" + context.admin_route + "/pages/" + page_route;
        } else {
            page_url = base_url + "/pages/" + page_route;
        }

        return page_url;
    }

    static tuFilter() {
        var args = Array.from(arguments);
        var numargs = args.length;
        var lang = undefined;

        if (numargs === 3 && Array.isArray(args[1]) || numargs === 2 && !Array.isArray(args[1])) {
            lang = args.pop();
        } else if (numargs === 2 && Array.isArray(args[1])) {
            var subs = args.pop();
            args = array_merge(args, subs);
        }

        return Grav.instance().admin.translate(args, lang);
    }

    toYamlFilter(value, inline = undefined) {
        return Yaml.dump(value, inline);
    }

    fromYamlFilter(value) {
        return Yaml.parse(value);
    }

    adminNicetimeFilter(date, long_strings = true) {
        return Grav.instance().admin.adminNiceTime(date, long_strings);
    }

}
