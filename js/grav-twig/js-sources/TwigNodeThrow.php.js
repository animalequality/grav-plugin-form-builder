//
//@package    Grav\Common\Twig
//
//@copyright  Copyright (C) 2015 - 2019 Trilby Media, LLC. All rights reserved.
//@license    MIT License; see LICENSE file for details.
//

//
//TwigNodeThrow constructor.
//@param int $code
//@param Node $message
//@param int $lineno
//@param string|null $tag
//
//
//Compiles the node to PHP.
//
//@param Compiler $compiler A Twig_Compiler instance
//@throws \LogicException
//
export class TwigNodeThrow extends Node {
    constructor(code, message, lineno = 0, tag = undefined) {
        super({
            message: message
        }, {
            code: code
        }, lineno, tag);
    }

    compile(compiler) {
        compiler.addDebugInfo(this);
        compiler.write("throw new \\RuntimeException(").subcompile(this.getNode("message")).write(", ").write(this.getAttribute("code") ? this.getAttribute("code") : 500).write(");\n");
    }

}
