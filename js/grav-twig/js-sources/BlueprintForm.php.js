//@var array
//@var string
//@var string
//@var array
//@var array
//
//Load file and return its contents.
//
//@param string $filename
//@return array
//
//
//Get list of blueprint form files (file and its parents for overrides).
//
//@param string|array $path
//@param string $context
//@return array
//
//
//Constructor.
//
//@param string|array $filename
//@param array $items
//
//
//Set filename for the blueprint. Can also be array of files for parent lookup.
//
//@param string|array $filename
//@return $this
//
//
//Get the filename of the blueprint.
//
//@return array|null|string
//
//
//Set context for import@ and extend@.
//
//@param $context
//@return $this
//
//
//Set custom overrides for import@ and extend@.
//
//@param array $overrides
//@return $this
//
//
//Load blueprint.
//
//@return $this
//
//
//Initialize blueprints with its dynamic fields.
//
//@return $this
//
//
//Get form.
//
//@return array
//
//
//Get form fields.
//
//@return array
//
//
//Extend blueprint with another blueprint.
//
//@param BlueprintForm|array $extends
//@param bool $append
//@return $this
//
//
//@param string $name
//@param mixed $value
//@param string $separator
//@param bool $append
//@return $this
//
//
//Get blueprints by using slash notation for nested arrays/objects.
//
//@example $value = $this->resolve('this/is/my/nested/variable');
//returns ['this/is/my', 'nested/variable']
//
//@param array  $path
//@param string  $separator
//@return array
//
//
//Deep merge two arrays together.
//
//@param array $a
//@param array $b
//@return array
//
//
//@param array $items
//@param array $path
//@return string
//
//
//@param array|string $value
//@return array|null
//
//
//@param array|string $value
//@param array $path
//
//
//Internal function that handles loading extended blueprints.
//
//@param array $files
//@param string|array|null $extends
//@return array
//
//
//Internal function to recursively load extended blueprints.
//
//@param string $filename
//@param array $parents
//@param array $extends
//@return array
//
//
//Internal function to reorder items.
//
//@param array $items
//@param array $keys
//@return array
//
export default class BlueprintForm {
    constructor(filename = undefined, items = Array()) {
        this.overrides = Array();
        this.dynamic = Array();
        this.nestedSeparator = "/";
        this.filename = filename;
        this.items = items;
    }

    setFilename(filename) {
        this.filename = filename;
        return this;
    }

    getFilename() {
        return this.filename;
    }

    setContext(context) {
        this.context = context;
        return this;
    }

    setOverrides(overrides) {
        this.overrides = overrides;
        return this;
    }

    load(_extends = undefined) //Only load and extend blueprint if it has not yet been loaded.
    {
        if (!this.items && this.filename) //Get list of files.
            //Load and extend blueprints.
            {
                var files = this.getFiles(this.filename);
                var data = this.doLoad(files, _extends);
                this.items = Array.from(data.shift() || "");

                for (var content of Object.values(data)) {
                    this.extend(content, true);
                }
            }

        this.deepInit(this.items);
        return this;
    }

    init() {
        {
            let _tmp_0 = this.dynamic;

            for (var key in _tmp_0) //Locate field.
            //Set dynamic property.
            {
                var data = _tmp_0[key];
                var path = key.split("/");
                var current = this.items;

                for (var field of Object.values(path)) {
                    if ("object" === typeof current) //Handle objects.
                        {
                            if (!(undefined !== current[field])) {
                                current[field] = Array();
                            }

                            current = current[field];
                        } else //Handle arrays and scalars.
                        {
                            if (!Array.isArray(current)) {
                                current = {
                                    [field]: Array()
                                };
                            } else if (!(undefined !== current[field])) {
                                current[field] = Array();
                            }

                            current = current[field];
                        }
                }

                for (var property in data) {
                    var call = data[property];
                    var action = "dynamic" + ucfirst(call.action);

                    if (method_exists(this, action)) {
                        this[action](current, property, call);
                    }
                }
            }
        }
        return this;
    }

    form() {
        return Array.from(this.get("form") || "");
    }

    fields() {
        var fields = this.get("form/fields");

        if (fields === undefined) {
            var field = this.get("form/field");
            fields = field !== undefined ? {
                "": Array.from(field || "")
            } : fields;
        }

        return Array.from(fields || "");
    }

    extend(__extends, append = false) {
        if (__extends instanceof self) {
            __extends = __extends.toArray();
        }

        if (append) {
            var a = this.items;
            var b = __extends;
        } else {
            a = __extends;
            b = this.items;
        }

        this.items = this.deepMerge(a, b);
        return this;
    }

    embed(name, value, separator = "/", append = false) {
        var oldValue = this.get(name, undefined, separator);

        if (Array.isArray(oldValue) && Array.isArray(value)) {
            if (append) {
                var a = oldValue;
                var b = value;
            } else {
                a = value;
                b = oldValue;
            }

            value = this.deepMerge(a, b);
        }

        this.set(name, value, separator);
        return this;
    }

    resolve(path, separator = "/") {
        var field;
        var fields = false;
        var parts = Array();
        var current = this["form/fields"];
        var result = [undefined, undefined, undefined];

        while ((field = current(path)) !== undefined) {
            if (!fields && undefined !== current.fields) {
                if (!!current.array) //Skip item offset.
                    {
                        result = [current, parts, path ? path.join(separator) : undefined];
                        parts.push(path.shift());
                    }

                current = current.fields;
                fields = true;
            } else if (undefined !== current[field]) {
                parts.push(path.shift());
                current = current[field];
                fields = false;
            } else if (undefined !== current[index = "." + field]) {
                parts.push(path.shift());
                current = current[index];
                fields = false;
            } else {
                break;
            }
        }

        return result;
    }

    deepMerge(a, b) {
        var bref_stack = [a];
        var head_stack = [b];

        do {
            end(bref_stack);
            var bref = bref_stack[key(bref_stack)];
            var head = head_stack.pop();
            delete bref_stack[key(bref_stack)];

            for (var key in head) {
                var value = head[key];

                if (strpos(key, "@") !== false) //Remove @ from the start and the end. Key syntax `import@2` is supported to allow multiple operations of the same type.
                    {
                        var list = key.replace(/^(@*)?([^@]+)(@\d*)?$/g, "\\2").split("-", 2);
                        var action = list.shift();
                        var property = list.shift();

                        switch (action) {
                            case "unset":
                            case "replace":
                                if (!property) {
                                    bref = {
                                        "unset@": true
                                    };
                                } else {
                                    delete bref[property];
                                }

                                continue;
                        }
                    }

                if (undefined !== key && undefined !== bref[key] && Array.isArray(bref[key]) && Array.isArray(head[key])) {
                    bref_stack.push(bref[key]);
                    head_stack.push(head[key]);
                } else {
                    bref = array_merge(bref, {
                        [key]: head[key]
                    });
                }
            }
        } while (head_stack.length);

        return a;
    }

    deepInit(items, path = Array()) {
        var ordering = "";
        var order = Array();
        var field = end(path) === "fields";

        for (var key in items) //Set name for nested field.
        {
            var item = items[key];

            if (field && undefined !== item.type) {
                item.name = key;
            }

            if (strpos(key, "@") !== false) //Remove @ from the start and the end. Key syntax `import@2` is supported to allow multiple operations of the same type.
                {
                    var list = key.replace(/^(@*)?([^@]+)(@\d*)?$/g, "\\2").split("-", 2);
                    var action = list.shift();
                    var property = list.shift();

                    switch (action) {
                        case "unset":
                            delete items[key];

                            if (!items) {
                                return undefined;
                            }

                            break;

                        case "import":
                            delete items[key];
                            this.doImport(item, path);
                            break;

                        case "ordering":
                            ordering = item;
                            delete items[key];
                            break;

                        default:
                            this.dynamic[path.join("/")][property] = {
                                action: action,
                                params: item
                            };
                    }
                } else if (Array.isArray(item)) //Recursively initialize form.
                {
                    var newPath = array_merge(path, [key]);
                    var location = this.deepInit(item, newPath);

                    if (location) {
                        order[key] = location;
                    } else if (location === undefined) {
                        delete items[key];
                    }
                }
        }

        delete item;

        if (order) //Reorder fields if needed.
            {
                items = this.doReorder(items, order);
            }

        return ordering;
    }

    loadImport(value) {
        var type = !("string" === typeof value) ? !(undefined !== value.type) ? undefined : value.type : value;
        var field = "form";

        if (type && strpos(type, ":") !== false) {
            [type, field] = type.split(":", 2);
        }

        if (!type && !field) {
            return undefined;
        }

        if (type) {
            var files = this.getFiles(type, undefined !== value.context ? value.context : undefined);

            if (!files) {
                return undefined;
            }

            var blueprint = new static(files);
            blueprint.setContext(this.context).setOverrides(this.overrides).load();
        } else {
            blueprint = this;
        }

        var _import = blueprint.get(field);
        return Array.isArray(_import) ? _import : undefined;
    }

    doImport(value, path) {
        var imported = this.loadImport(value);

        if (imported) {
            this.deepInit(imported, path);
            var name = path.join("/");
            this.embed(name, imported, "/", false);
        }
    }

    doLoad(files, __extends = undefined) {
        var filename = files.shift();
        var content = this.loadFile(filename);
        var key = "";

        if (undefined !== content["extends@"]) {
            key = "extends@";
        } else if (undefined !== content["@extends"]) {
            key = "@extends";
        } else if (undefined !== content["@extends@"]) {
            key = "@extends@";
        }

        var override = !!__extends;
        __extends = Array.from((key && !__extends ? content[key] : __extends) || "");
        delete content["extends@"];
        var data = __extends ? this.doExtend(filename, files, __extends, override) : Array();
        data.push(content);
        return data;
    }

    doExtend(filename, parents, __extends, override = false) //TODO: In PHP 5.6+ use array_merge(...$data);
    {
        if ("string" === typeof key(__extends)) {
            __extends = [__extends];
        }

        var data = [Array()];

        for (var value of Object.values(__extends)) //Accept array of type and context or a string.
        {
            var type = !("string" === typeof value) ? !(undefined !== value.type) ? undefined : value.type : value;

            if (!type) {
                continue;
            }

            if (type === "@parent" || type === "parent@") {
                if (!parents) {
                    throw new RuntimeException(`Parent blueprint missing for '${filename}'`);
                }

                var files = parents;
            } else {
                files = this.getFiles(type, undefined !== value.context ? value.context : undefined);

                if (override && !files) {
                    throw new RuntimeException(`Blueprint '${type}' missing for '${filename}'`);
                }

                if (files && array_intersect(files, parents)) //Let's check if user really meant __extends@: parent@.
                    {
                        var index = array_search(filename, files, true);

                        if (index !== false) //We want to grab only the parents of the file which is currently being loaded.
                            {
                                files = files.slice(index + 1);
                            }

                        if (files !== parents) {
                            throw new RuntimeException(`Loop detected while extending blueprint file '${filename}'`);
                        }

                        if (!parents) {
                            throw new RuntimeException(`Parent blueprint missing for '${filename}'`);
                        }
                    }
            }

            if (files) {
                data.push(this.doLoad(files));
            }
        }

        return call_user_func_array("array_merge", data);
    }

    doReorder(items, keys) {
        var reordered = Object.keys(items);

        for (var item in keys) {
            var ordering = keys[item];

            if (String(+(ordering === String(ordering)))) {
                var location = array_search(item, reordered, true);
                var rel = reordered.splice(location, 1);
                reordered.splice(ordering, 0, rel);
            } else if (undefined !== items[ordering]) {
                location = array_search(item, reordered, true);
                rel = reordered.splice(location, 1);
                location = array_search(ordering, reordered, true);
                reordered.splice(location + 1, 0, rel);
            }
        }

        return array_merge(array_flip(reordered), items);
    }

}
