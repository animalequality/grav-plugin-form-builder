//
//@package    Grav\Common\Twig
//
//@copyright  Copyright (C) 2015 - 2019 Trilby Media, LLC. All rights reserved.
//@license    MIT License; see LICENSE file for details.
//

//
//TwigNodeTryCatch constructor.
//@param Node $try
//@param Node|null $catch
//@param int $lineno
//@param string|null $tag
//
//
//Compiles the node to PHP.
//
//@param Compiler $compiler A Twig_Compiler instance
//@throws \LogicException
//
export class TwigNodeTryCatch extends Node {
    constructor(try_r, catch_r = undefined, lineno = 0, tag = undefined) {
        super({
            try: try_r,
            catch: catch_r
        }, Array(), lineno, tag);
    }

    compile(compiler) {
        compiler.addDebugInfo(this);
        compiler.write("try {");
        compiler.indent().subcompile(this.getNode("try"));

        if (this.hasNode("catch") && undefined !== this.getNode("catch")) {
            compiler.outdent().write("} catch (\\Exception $e) {" + "\n").indent().write("if (isset($context['grav']['debugger'])) $context['grav']['debugger']->addException($e);" + "\n").write("$context['e'] = $e;" + "\n").subcompile(this.getNode("catch"));
        }

        compiler.outdent().write("}\n");
    }

}
