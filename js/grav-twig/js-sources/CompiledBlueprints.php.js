//
//@package    Grav\Common\Config
//
//@copyright  Copyright (C) 2015 - 2019 Trilby Media, LLC. All rights reserved.
//@license    MIT License; see LICENSE file for details.
//

//
//Returns checksum from the configuration files.
//
//You can set $this->checksum = false to disable this check.
//
//@return bool|string
//
//
//Create configuration object.
//
//@param array  $data
//
//
//Get list of form field types.
//
//@return array
//
//
//Finalize configuration object.
//
//
//Load single configuration file and append it to the correct position.
//
//@param  string  $name  Name of the position.
//@param  array   $files  Files to be loaded.
//
//
//Load and join all configuration files.
//
//@return bool
//@internal
//
export default class CompiledBlueprints extends CompiledBase {
    constructor(cacheFolder, files, path) {
        super(cacheFolder, files, path);
        this.version = 2;
    }

    checksum() {
        if (undefined === this.checksum) {
            this.checksum = md5(JSON.stringify(this.files) + JSON.stringify(this.getTypes()) + this.version);
        }

        return this.checksum;
    }

    createObject(data = Array()) {
        this.object = new BlueprintSchema(data).setTypes(this.getTypes());
    }

    getTypes() {
        return Grav.instance().plugins.formFieldTypes ? Grav.instance().plugins.formFieldTypes : Array();
    }

    finalizeObject() {}

    loadFile(name, files) //Load blueprint file.
    {
        var blueprint = new Blueprint(files);
        this.object.embed(name, blueprint.load().toArray(), "/", true);
    }

    loadFiles() //Convert file list into parent list.
    //@var array $files
    //Load files.
    {
        this.createObject();
        var list = Array();

        for (var files of Object.values(this.files)) {
            for (var name in files) {
                var item = files[name];
                list[name].push(this.path + item.file);
            }
        }

        for (var name in list) {
            var files = list[name];
            this.loadFile(name, files);
        }

        this.finalizeObject();
        return true;
    }

    getState() {
        return this.object.getState();
    }

}
