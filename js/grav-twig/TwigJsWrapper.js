/**
 * Copyright (C) 2019, 2020, Raphaël . Droz + floss @ gmail DOT com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 */

class Empty {}

class AbstractTokenParser {
    constructor() {
        Twig.extend((Twig) => {
            // console.log(`extending with tag ${this.getTag()}`);
            Twig.exports.extendTag({
                type: this.getTag(),
                parse: this.parse,
                compile: () => null,
                // regex: /^flag\s+(.+)$/,
                next: [ ],
                open: true,
            });
        });
    }
};

class Twig_Extension {
    init() {
        // This returns a set of new Twig_SimpleFilter/Function/Parsers
        // each of them being appended to `Twig` during instanciation.
        this.getFilters();
        this.getFunctions();
        if (this.getTokenParsers) {
            this.getTokenParsers();
        }
    }
};

class Twig_SimpleFunction {
    constructor(name, callback, options) {
        Twig.extendFunction(name, (...args) => {
            if (typeof options === 'object') {
                if (options.needs_context) {
                    args.unshift(Twig);
                }
                if (options.needs_environment) {
                    args.unshift(null);
                }
            }

            try {
                var opts = JSON.stringify(options) || '';
                console.log(`TWIG function ${name} (${opts}), context: `, callback, "arguments:", ...args);
                if (typeof callback === 'function') {
                    return callback(...args);
                } else if (typeof callback === 'string') {
                    return args[0];// A PHP builtin.
                } else {
                    return callback[0][callback[1]].bind(callback[0])(...args);
                }
            } catch(e) {
                console.warn("filter-error-handler: ", e);
                throw(e);
            }
        });
    }
};

class Twig_SimpleFilter {
    constructor(name, callback, options) {
        Twig.extendFilter(name, (...args) => {
            if (typeof options === 'object') {
                if (options.needs_context) {
                    args.unshift(null);
                }
                if (options.needs_environment) {
                    args.unshift(null);
                }
            }

            try {
                var opts = JSON.stringify(options) || '';
                console.log(`TWIG filter ${name} (${opts}), context: `, callback, "arguments:", ...args);
                if (typeof callback === 'function') {
                    return callback(...args);
                } else if (typeof callback === 'string') {
                    return args[0];// A PHP builtin.
                } else {
                    return callback[0][callback[1]].bind(callback[0])(...args);
                }
            } catch(e) {
                console.warn("filter-error-handler: ", e);
                throw(e);
            }
        });
    }
}

export {
    Empty, // For Node, Token, Twig_Loader_Filesystem
    AbstractTokenParser,
    Twig_Extension,
    Twig_SimpleFunction,
    Twig_SimpleFilter,
};
