#!/usr/bin/env php
<?php

// Console
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Application;

// Reflector
use Phpactor\WorseReflection\ReflectorBuilder;
use Phpactor\WorseReflection\Core\SourceCode as WorseSourceCode;

// Renammer
use Phpactor\CodeTransform\Adapter\TolerantParser\Refactor\TolerantRenameVariable;
use Phpactor\CodeTransform\Domain\SourceCode;

require_once(__DIR__ . '/../../vendor/autoload.php');

class RenamerCommand extends Command
{

    protected function configure()
    {
        $this->setName("rename")
             ->setDescription("Rename variable across a PHP file.")
             ->addArgument('file', InputArgument::REQUIRED, 'The file to process.')
            // Adding "class" to that list is blocked by https://github.com/phpactor/worse-reflection/pull/66
             ->addOption('blacklist', 'b', InputOption::VALUE_REQUIRED, 'Variables to modify', 'var,default,break,function,try,catch')
             ->addOption('output', 'o', InputOption::VALUE_REQUIRED, 'Rewritten sourcecode output path. Default: stdout.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $source = file_get_contents($input->getArgument('file'));
        $callback = [$this, 'replace'];
        $blacklist = array_filter(explode(',', $input->getOption('blacklist')));
        if (!$blacklist) {
            $output->writeln('No variable to rename');
            return 1;
        }

        $output->writeln('Renaming variables: ' . implode(',', $blacklist), OutputInterface::VERBOSITY_VERBOSE);
        $iterations = $this->continuousSearch($source, $blacklist, $callback, $output);
        if ($input->getOption('output')) {
            file_put_contents($input->getOption('output'), $source);
        } else {
            $output->writeln($source);
        }

        return 0;
    }

    private function continuousSearch(string &$source, ...$arguments)
    {
        [$blacklist, $callback, $output] = $arguments;
        $iterations = 0;
        do {
            if ($iterations) {
                $output->writeln("# Run " . ($iterations + 1), OutputInterface::VERBOSITY_VERBOSE);
            }
            $didSub = $this->search($source, ...$arguments);
            file_put_contents('/tmp/temp.' . $iterations, $source);
            $iterations++;
        } while ($didSub);

        return $iterations;
    }

    public function replace(string $sourceCode, $offset, $newName)
    {
        // $sourceCode = SourceCode::fromString(file_get_contents($file));
        $renamer = new TolerantRenameVariable();
        printf("Replacing on source %d long\n", strlen($sourceCode));
        return $renamer->renameVariable(
            SourceCode::fromString((string)$sourceCode),
            $offset,
            $newName
        );
    }

    public function search(string &$source, array $blacklist, $callback, ...$arguments)
    {
        [$output] = $arguments;
        $sourceCode = WorseSourceCode::fromString($source);
        $build = ReflectorBuilder::create()->addSource($sourceCode)->build();
        $classes = $build->reflectClassesIn($sourceCode)->keys();

        foreach ($classes as $class_name) {
            $class = $build->reflectClass($class_name);

            foreach ($class->methods() as $method) {
                $method_name = $method->name();
                $output->writeln("method: " . $method_name, OutputInterface::VERBOSITY_VERBOSE);
                $output->writeln($method->body(), OutputInterface::VERBOSITY_DEBUG);

                foreach ($method->frame()->locals() as $var) {
                    $name = $var->name();
                    $offset = $var->offset()->toInt();
                    if (in_array($name, $blacklist, true)) {
                        $output->writeln("\t local: " . $name . "\t" . $offset, OutputInterface::VERBOSITY_VERBOSE);
                        $source = $callback($sourceCode, $offset, $name . '_r');
                        return true;
                    }
                }

                // Needs for parameters which are not used by function's body
                foreach ($method->parameters() as $var) {
                    $name = $var->name();
                    $offset = $var->position()->fullStart();
                    if (in_array($name, $blacklist, true)) {
                        $output->writeln("\t params: " . $name . "\t" . $offset, OutputInterface::VERBOSITY_VERBOSE);
                        $source = $callback($sourceCode, $offset, $name . '_r');
                        return true;
                    }
                }
            }
        }

        return false;
    }
}

$app = new Application();
$app->add(new RenamerCommand());
$app->run();
