// Waiting for:
// * https://github.com/twigjs/twig.js/issues/694
global.XMLHttpRequest = require('unxhr').XMLHttpRequest;
global.window = {};
const Twig = require('twig'), twig = Twig.twig;
const marked = require('marked');

import { Grav } from './Grav.js';
global.Grav = new Grav();

const x = require('./TwigJsWrapper.js');
global.Twig_Extension = x.Twig_Extension;
global.AbstractTokenParser = x.AbstractTokenParser;
global.Twig_SimpleFunction = x.Twig_SimpleFunction,
global.Twig_SimpleFilter = x.Twig_SimpleFilter;

const AdminTwigExtension = require('./js-sources/AdminTwigExtension.php.js'),
      TwigExtension = require('./js-sources/TwigExtension.php.js'),
      Utils = require('./js-sources/Utils.php.js');
global.Utils = new Utils();

const twigExtension = new TwigExtension(), adminTwigExtension = new AdminTwigExtension();

// dup
global.Utils.processMarkdown = marked;
global.rtrim = (s, v = '\\s') => s.replace(new RegExp("[" + v + "]*$"), '');
global.INPUT_COOKIE = typeof document === 'object' ? document.cookie : '',
global.FILTER_SANITIZE_STRING = '',
global.filter_input = (input, key, mode = 'FILTER_SANITIZE_STRING') => input[key];
// /dup


const tests = require('./tests.js');
const base = "http://localhost:8000/templates";
for(const [k, v] of Object.entries(tests)) {
    v.name = k;
    var template = twig({
        href: `${base}/forms/fields/${v.type}/${v.type}.html.twig`,
        base,
        async: false
    });

    console.log(template.render({form: {messages: {}}, scope: '', field: v}));
}
