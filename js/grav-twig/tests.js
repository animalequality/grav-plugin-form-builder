module.exports = {
    "city": {
        "label": "City",
        "type": "text",
        "id": "cityID",
        "classes": "light",
        "validate": {
            "min": 2,
            "max": 35,
            "required": true
        }
    },
    "country": {
        "label": "Country",
        "type": "select",
        "id": "countryID",
        "classes": "light",
        "default": "FI",
        "options": {
            "FI": "Finland"
        },
        "validate": {
            "required": true
        }
    },
    "postalCode": {
        "label": "Zip code",
        "type": "text",
        "id": "postalCodeID",
        "classes": "light",
        "size": 10,
        "help": "zip code",
        "description": "",
        "validate": {
            "min": 2,
            "max": 10,
            "message": "Please enter a valid zip code",
            "required": true
        }
    },
    "email": {
        "label": "Email",
        "type": "email",
        "id": "emailID",
        "classes": "light",
        "validate": {
            "type": "email",
            "pattern": "^\\w([\\w\\.+-]*[\\w-])?@([\\w-]+\\.)+[a-z]{2,5}$",
            "required": "true",
            "message": "Please enter a valid email"
        }
    }
};
