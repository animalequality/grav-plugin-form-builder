const { TwingFilter, TwingFunction } = require('twing');

/**
 * Map PHP callback structure callback being [Object, method] instead of a function
 * which return Promise.resolve(result);
 */
class Twig_SimpleFunction extends TwingFunction {
    constructor(name, callback, options) {
        super(name,
              typeof options === 'object' && options.needs_environment ?
              (env, string) => Promise.resolve(callback[0][callback[1]](env, string))
              : (string) => Promise.resolve(callback[0][callback[1]](string)),
              options);
    }
};

class Twig_SimpleFilter extends TwingFilter {
    constructor(name, callback, options) {
        // console.log("setup filter %s with %s", name, typeof callback[0][callback[1]]);
        if (typeof options === 'object' && options.needs_environment && options.needs_context) {
            super(name, (env, ctx, string) => {
                return Promise.resolve(callback[0][callback[1]](env, ctx, string));
            }, options);
        } else if (typeof options === 'object' && options.needs_environment) {
            super(name, (env, string) => {
                // console.log("calling env-filter %s", name, env);
                // Bug? That should be callback[0][callback[1]](env, string)
                // https://github.com/NightlyCommit/twing/blob/master/docs/advanced.md#environment-aware-filters
                return Promise.resolve(callback[0][callback[1]](string, env));
            }, options);
        } else if (typeof options === 'object' && options.needs_context) {
            super(name, (ctx, string) => {
                return Promise.resolve(callback[0][callback[1]](ctx, string));
            }, options);
        }  else {
            super(name, (string) => {
                // console.log("calling filter %s %s", name, typeof callback[0][callback[1]]);
                return Promise.resolve(callback[0][callback[1]](string));
            }, options);
        }
    }
};

module.exports = {
    Twig_SimpleFunction,
    Twig_SimpleFilter
};
