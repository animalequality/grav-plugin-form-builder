// * https://github.com/NightlyCommit/twing/issues/469 (fixed in 4.0.2)
// Waiting for
// * https://github.com/NightlyCommit/twing/issues/388
const { TwingEnvironment, TwingTokenParser, TwingLoaderFilesystem, TwingLoaderArray, TwingExtension } = require('twing');
global.Twig_Extension = TwingExtension,
global.Twig_Loader_Filesystem = TwingLoaderFilesystem,
global.Twig_Environment = TwingEnvironment,
global.AbstractTokenParser = TwingTokenParser;

const { Twig_SimpleFunction, Twig_SimpleFilter } = require('./TwingWrapper.js');
global.Twig_SimpleFunction = Twig_SimpleFunction,
global.Twig_SimpleFilter = Twig_SimpleFilter;

const Grav = require('./Grav.js');
global.Grav = new Grav();

const jsyaml = require('js-yaml');
const marked = require('marked');

jsyaml.dump = jsyaml.safeDump;
jsyaml.load = jsyaml.safeLoad;

global.Yaml = jsyaml,

global.rtrim = (s, v = '\\s') => s.replace(new RegExp("[" + v + "]*$"), ''),
global.INPUT_COOKIE = typeof document === 'object' ? document.cookie : '',
global.FILTER_SANITIZE_STRING = '',
global.filter_input = (input, key, mode = 'FILTER_SANITIZE_STRING') => input[key];


const AdminTwigExtension = require('./js-sources/AdminTwigExtension.php.js'),
      TwigExtension = require('./js-sources/TwigExtension.php.js'),
      Utils = require('./js-sources/Utils.php.js');

global.Utils = new Utils();
global.Utils.processMarkdown = marked;


let loader = new TwingLoaderFilesystem('../../../form/templates/');
const twing = new TwingEnvironment(loader, {debug: true});
twing.addExtension(new TwigExtension(), 'TwigExtension');
twing.addExtension(new AdminTwigExtension(), 'AdminTwigExtension');

const tests = require('./tests.js');
for(const [k, v] of Object.entries(tests)) {
    v.name = k;
    var y = twing.render(`forms/fields/${v.type}/${v.type}.html.twig`, {scope: '', field: v}).then(x => console.log("x", x));
}
