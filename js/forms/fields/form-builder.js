let source_grid, dest_grid, store;

function elementFromSelector(s) {
    return typeof s === 'string' ? document.querySelector(s) : s;
}

function gridFromSelector(s) {
    var h = elementFromSelector(s);
    return h.gridstack ? h.gridstack : h;
}

function distributeItems(s, d, config) {
    if (typeof config === 'string') {
        console.warn('Invalid predefined configuration');
        return false;
    }

    if (! config) {
        config = [];
    }

    const source = elementFromSelector(s),
          destination = gridFromSelector(d);

    destination.batchUpdate();
    destination.removeAll();
    var items = GridStack.Utils.sort(config);
    items.forEach(function (node) {
        var item = source.querySelector(`[data-key="${node.key}"]`);
        destination.addWidget(item.outerHTML, node);
        source_grid.removeWidget(item);
    });
    destination.commit();

    return false;
};

/*
GridStack.prototype._writeAttr_save = GridStack.prototype._writeAttr;
GridStack.prototype._writeAttr = function(el, node) {
    console.log("Grid is ", this.listGridItems(this.engine.nodes).join(', '));
    console.log("write attr from", el.dataset, " to ", node);
    this._writeAttr_save(el, node);
};
*/

GridStack.prototype.logEnabled = function() {
    this.on('added removed change', (e, items = []) => {
        var str = this.listGridItems(items).join(', ');
        console.log(`event: "${e.type}" on "${this.$el.attr('id')}" with ${items.length} items. ${str}`);
    });
};

GridStack.prototype.listGridItems = function(items = []) {
    return items.map(function (node) {
        var k = node.el ? node.el.dataset.key : 'UNDEF';
        var suffix = node.width == 1 && node.height == 1 ? ']' : `_${node.width},${node.height}]`;
        return `${k}[${node.x},${node.y}${suffix}`;
    });
};

GridStack.prototype.getGridItems = function() {
    return this.engine.nodes.map(function (node) {
        // It's unexpected that an element has no widget attached anymore
        var v = {
            key: node.el ? node.el.dataset.key : 'UNDEF',
            x: node.x,
            y: node.y,
            width: node.width,
            height: node.height
        };

        /**
         * In case we want to store extra-values with the ordering instead of within the form
         * (NB: may not be adequate)
         */
        var values = {};
        $(node.el).find('input:not([name*="_required"])').each(function(i) {
            values[$(this).attr('name')] = $(this).val();
        });
        if (Object.keys(values).length) {
            // v['value'] = values;
        }

        return v;
    });
};

GridStack.prototype.save = function(store) {
    var data = this.getGridItems();
    data = GridStack.Utils.sort(data);
    console.log("saving Grid: ", this.listGridItems(this.engine.nodes).join(', '));
    store.val(JSON.stringify(data));
};

function renderWidget(id, type, extra_data) {
    return new Promise((resolve, reject) => {
        if (gravjs) {
            gravjs.Blueprint.renderRemoteField(`/user/plugins/form-builder/blueprints/fields/${type}/${type}.yaml`,
                                               '/user/plugins/form/templates').then(e => resolve(e.join("\n")));
        } else {
            resolve(`<div>#${id} <input id="collect-${id}" name="${scope || ''}[${id}]" type="${type}" /></div>`);
        }
    });
}

function initGrids(evt, source_sel = '#grid1', dest_sel = '#grid2') {
    var options = {
        cellHeight: '5em',
        float: false,
        removeTimeout: 100,
        acceptWidgets: true,
    };

    [source_grid, dest_grid] = GridStack.initAll(options);

    source_grid.enableResize(false);
    source_grid.column(1);

    dest_grid.placeholderText = 'Drop here';
    dest_grid.maxRow = 5;
    dest_grid.column(2);
    dest_grid.float(true);

    store = $(dest_sel).siblings('.gridstack-store');
    var config = JSON.parse(store.val());
    if (typeof config === 'object') {
        distributeItems(source_sel, dest_grid, config);
    }

    source_grid.logEnabled();
    dest_grid.logEnabled();
    addEvents(source_sel, dest_sel);
}

function addEvents(source_sel, dest_sel) {
    dest_grid.on('added change removed', function(event, items) {
        dest_grid.save(store);
    });

    /**
     * #grid2.removed event is often not triggered, especially after items have
     * been dragged back & forth a couple of times.
     * So we additionally rely on the #grid1.change event.
    */
    source_grid.on('change', function(event, items) {
        dest_grid.save(store);
    });

    /*
    dest_grid.on('dropped', function(event, previousWidget, newWidget) {
        console.log('Removed widget that was dragged out of grid:', previousWidget.el.dataset.key);
        console.log('Added widget in dropped grid:', newWidget.el.dataset.key);
    });
    */

    // For later v2
    $('.newWidget').click(dest_grid, addCustomWidget);
    $(source_sel + ', ' + dest_sel).on('change', 'select input', function(event) {
        console.log("input fields changed");
        dest_grid.save(store);
    });
}

window.addEventListener('DOMContentLoaded', initGrids);

// For later v2
function addCustomWidget(gridstack) {
    var $parent = $(this).parent(),
        $element_id_e = $parent.find('input[name="element-id"]'),
        element_id = $element_id_e.val(),
        element_type = $parent.find('select[name="element-type"] option:selected').val();

    if (element_id) {
        if ($(`collect-${element_id}`).length) {
            return false;
        }

        // console.log(gridstack);
        renderWidget(element_id, element_type).then((field_markup) => {
            var markup = `<div class="grid-stack-item" data-key="${element_id}"><div class="grid-stack-item-content">${field_markup}</div></div>`;
            gridstack.addWidget(markup);
            $element_id_e.val('');
        });
    }

    return false;
}
