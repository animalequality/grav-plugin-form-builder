/**
 * Copyright (C) 2019, 2020 Raphaël . Droz + floss @ gmail DOT com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 */

/**
 * Current limitations from both side of the Twig interpreters:
 *
 * https://github.com/twigjs/twig.js/issues/277 (Support string interpolation #277)
 * https://github.com/NightlyCommit/twing/issues/388 (Added a web loader #430)
 */
import Twig from 'twig';
import yaml from 'yaml';
import marked from 'marked';
import AdminTwigExtension from '../grav-twig/js-sources/AdminTwigExtension.php.js';
import TwigExtension from '../grav-twig/js-sources/TwigExtension.php.js';
import Utils from '../grav-twig/js-sources/Utils.php.js';
import { Grav } from '../grav-twig/Grav.js';
import { Blueprint as Bp } from '../grav-twig/Blueprint.js';

Utils.prototype.getNonce = () => '';
Utils.prototype.generateRandomString = () => '';
window.Utils = new Utils();

TwigExtension.prototype.yamlEncodeFilter = (data, inline = 10) => yaml.dump(data, inline);
TwigExtension.prototype.yamlDecodeFilter = data => yaml.load(data);
TwigExtension.prototype.arrayFilter = (input) => Array.from(input ? input : []);
TwigExtension.prototype.evaluateTwigFunc = function(ctx, expr) {
    var x = Twig.twig({
        data: expr
    }).render(ctx);
    console.log("evaluateTwigFunc returned", x);
    return x;
};

const twigExtension = new TwigExtension(), adminTwigExtension = new AdminTwigExtension();
twigExtension.init();
adminTwigExtension.init();

// dup
window.Utils.processMarkdown = function(a, b) {
    return marked(a || "", b);
};

window.html_entity_decode = function(message) {
  return message.replace(/[<>'"]/g, function(m) {
    return '&' + {
      '\'': 'apos',
      '"': 'quot',
      '&': 'amp',
      '<': 'lt',
      '>': 'gt',
    }[m] + ';';
  });
};

window.INPUT_COOKIE = typeof document === 'object' ? document.cookie : '',
window.FILTER_SANITIZE_STRING = '',
window.filter_input = (input, key, mode = 'FILTER_SANITIZE_STRING') => input[key];
// /dup

// twig_vars
const config = new class{ get() {}}, active_language = null, locator = new class { findResource() {} }, pages = new class{ homeUrl() {}; baseUrl() {};}, ROOT_DIR = '',
      twig_vars = {
          grav              : Grav,
          config            : config,
          system            : config.get('system'),
          theme             : config.get('theme'),
          site              : config.get('site'),
          uri               : Grav['uri'],
          assets            : Grav['assets'],
          taxonomy          : Grav['taxonomy'],
          browser           : Grav['browser'],
          base_dir          : ROOT_DIR.trimRight('/'),
          home_url          : pages.homeUrl(active_language),
          base_url          : pages.baseUrl(active_language),
          base_url_absolute : pages.baseUrl(active_language, true),
          base_url_relative : pages.baseUrl(active_language, false),
          base_url_simple   : Grav['base_url'],
          theme_dir         : locator.findResource('theme://'),
          theme_url         : Grav['base_url'] + '/' + locator.findResource('theme://', false),
          html_lang         : Grav['language'].getActive() ? config.get('site.default_lang', 'en') : '',
          language_codes    : '' // new LanguageCodes,
      };

const Field = {
    /**
     * Resolve as a Twig.JS template having a template.render(data) method.
     * But if data is set, then the Promise resolves with the rendered ouput.
     */
    get: (type, base = './', data = false) => {
        console.log(`Loading URL at ${type} (${base}) with data=${data}`);
        return new Promise(
            (resolve, reject) => Twig.twig({
                // debug: true,
                // trace: true,
                href: `${base}/forms/fields/${type}/${type}.html.twig`,
                base,
                //async: false,
                load: template => resolve(data ? template.render(data) : template)
            })
        );
    },

    getForm: (base = './', data = false) => {
        console.log(`Loading Form template at ${base} with data=${data}`);
        return new Promise(
            (resolve, reject) => Twig.twig({
                // debug: true, trace: true,
                href: `${base}/form.html.twig`,
                base,
                //async: false,
                load: template => resolve(data ? template.render(data) : template)
            })
        );
    }

};


const Blueprint = {
    get: (url) => {
        return fetch(url).then(e => e.text());
    },

    renderRemoteField: function (url, ...args) {
        return this.get(url).then(e => this.renderField(e, ...args));
    },

    renderRemoteForm: function (url, ...args) {
        return this.get(url).then(e => this.renderForm(e, ...args));
    },

    renderField: async(blueprint, ...args) => {
        var as_json = yaml.load(blueprint);
        var fields = [];
        for (const [name, field] of Object.entries(as_json.form.fields)) {
            var type = field.field ? field.field.type : field.type;
            field.name = field.name || name;
            fields.push(await Field.get(type, ...args).then(t => {
                return t.render({field: field, ...twig_vars});
            }));
        }
        console.log(`Fields: ${fields}`);

        return fields;
    },

    renderForm: async(blueprint, ...args) => {
        var as_json = yaml.load(blueprint),
            data = {form: as_json.form, ...twig_vars},
            bp = new Bp('my-name', {form: as_json.form, rules: as_json.form.rules});
        console.log(bp);
        var f = await Field.getForm(...args).then(t => {
            return t.render(data);
        });
        console.log(`Form: ${f}`);
        return f;
    }
};

// gravjs.Blueprint.renderRemote("/myforms/blueprints/partials/form.yaml", 'form/templates').then(e => console.log(e));
export {
    Field,
    Blueprint
};
