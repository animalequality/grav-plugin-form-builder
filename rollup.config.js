// ToDo: @jawish/rollup-plugin-twig ?
// ToDo: https://github.com/mishoo/tweeg.js ?

// import alias from '@rollup/plugin-alias';
// import externalGlobals from 'rollup-plugin-external-globals';
// import json from 'rollup-plugin-json';
// import pkg from './package.json';
// import polyfill from 'rollup-plugin-polyfill';

import babel from 'rollup-plugin-babel';
import commonjs from 'rollup-plugin-commonjs';
import inject from '@rollup/plugin-inject';
import nodent from 'rollup-plugin-nodent';
import path from 'path';
import resolve from 'rollup-plugin-node-resolve';
import { terser } from "rollup-plugin-terser";
import visualizer from 'rollup-plugin-visualizer';

const isProduction = process.env.NODE_ENV === 'production';

function plugins(compress = false) {
    return [
        /*
        alias({
            'marked': path.resolve('node_modules/marked/lib/marked.esm.js')
        }),
        resolve({
            // preferBuiltins: true,
        }),
        externalGlobals({jquery: "$"}),
        polyfill(['whatwg-fetch']),
        */

        inject({
            // Incomaptible with commonjs()
            // See https://github.com/rollup/plugins/issues/92
            include: 'js/**/*.js',

            // fake objects to make it just-work
            AbstractTokenParser: [path.resolve('js/grav-twig/TwigJsWrapper.js'), 'AbstractTokenParser'],
            Node: [path.resolve('js/grav-twig/TwigJsWrapper.js'), 'Empty'],
            Token: [path.resolve('js/grav-twig/TwigJsWrapper.js'), 'Empty'],
            Twig_Loader_Filesystem: [path.resolve('js/grav-twig/TwigJsWrapper.js'), 'Empty'],
            Twig_Environment: [path.resolve('js/grav-twig/TwigJsWrapper.js'), 'Empty'],

            // other injections
            TwigNodeMarkdown: [path.resolve('js/grav-twig/js-sources/TwigNodeMarkdown.php.js'), 'TwigNodeMarkdown'],
            TwigNodeRender: [path.resolve('js/grav-twig/js-sources/TwigNodeRender.php.js'), 'TwigNodeRender'],
            TwigNodeScript: [path.resolve('js/grav-twig/js-sources/TwigNodeScript.php.js'), 'TwigNodeScript'],
            TwigNodeStyle: [path.resolve('js/grav-twig/js-sources/TwigNodeStyle.php.js'), 'TwigNodeStyle'],
            TwigNodeSwitch: [path.resolve('js/grav-twig/js-sources/TwigNodeSwitch.php.js'), 'TwigNodeSwitch'],
            TwigNodeThrow: [path.resolve('js/grav-twig/js-sources/TwigNodeThrow.php.js'), 'TwigNodeThrow'],
            TwigNodeTryCatch: [path.resolve('js/grav-twig/js-sources/TwigNodeTryCatch.php.js'), 'TwigNodeTryCatch'],
            TwigTokenParserMarkdown: [path.resolve('js/grav-twig/js-sources/TwigTokenParserMarkdown.php.js'), 'TwigTokenParserMarkdown'],
            TwigTokenParserRender: [path.resolve('js/grav-twig/js-sources/TwigTokenParserRender.php.js'), 'TwigTokenParserRender'],
            TwigTokenParserScript: [path.resolve('js/grav-twig/js-sources/TwigTokenParserScript.php.js'), 'TwigTokenParserScript'],
            TwigTokenParserStyle: [path.resolve('js/grav-twig/js-sources/TwigTokenParserStyle.php.js'), 'TwigTokenParserStyle'],
            TwigTokenParserSwitch: [path.resolve('js/grav-twig/js-sources/TwigTokenParserSwitch.php.js'), 'TwigTokenParserSwitch'],
            TwigTokenParserThrow: [path.resolve('js/grav-twig/js-sources/TwigTokenParserThrow.php.js'), 'TwigTokenParserThrow'],
            TwigTokenParserTryCatch: [path.resolve('js/grav-twig/js-sources/TwigTokenParserTryCatch.php.js'), 'TwigTokenParserTryCatch'],

            Twig_Extension: [path.resolve('js/grav-twig/TwigJsWrapper.js'), 'Twig_Extension'],

            Twig_SimpleFunction: [path.resolve('js/grav-twig/TwigJsWrapper.js'), 'Twig_SimpleFunction'],
            Twig_SimpleFilter: [path.resolve('js/grav-twig/TwigJsWrapper.js'), 'Twig_SimpleFilter'],
            Grav: [path.resolve('js/grav-twig/Grav.js'), 'Grav'],

            json_decode: [path.resolve('node_modules/locutus/php/json/json_decode.js'), 'json_decode'],
            // html_entity_decode: [path.resolve('node_modules/locutus/php/strings/html_entity_decode.js'), 'html_entity_decode'],
            range: [path.resolve('node_modules/locutus/php/array/range.js'), 'range'],
            str_replace: [path.resolve('node_modules/locutus/php/strings/str_replace.js'), 'str_replace'],
            is_numeric: [path.resolve('node_modules/locutus/php/var/is_numeric.js'), 'is_numeric'],
            array_merge: [path.resolve('node_modules/locutus/php/array/array_merge.js'), 'array_merge'],

        }),

        commonjs({
            // include: 'node_modules/twig/**',
            // ignoreGlobal: true,
            namedExports: {
                'node_modules/locutus/php/json/json_decode.js': ['json_decode'],
                // 'node_modules/locutus/php/strings/html_entity_decode.js': ['html_entity_decode'],
                'node_modules/locutus/php/array/range.js': ['range'],
                'node_modules/locutus/php/strings/str_replace.js': ['str_replace'],
                'node_modules/locutus/php/var/is_numeric.js': ['is_numeric'],
                'node_modules/locutus/php/array/array_merge.js': ['array_merge'],
            },
        }),

        nodent({
            promises: true,
            noRuntime: true
        }),

        babel({
            babelrc: false,
            presets: [['env', { modules: false, useBuiltIns: "usage", corejs: 3, targets: {ie: '11'}}]],
            runtimeHelpers: true,
            plugins: ['external-helpers', 'transform-object-rest-spread']
        }),

        !compress ? visualizer() : null,
        compress ? terser() : null,
    ];
}

export default [
    {
        input: 'js/forms/grav.fields.js',
        plugins: plugins(),
        output: [
            {
                file: 'js/build/grav.field.umd.js',
                format: 'umd',
                name: 'gravjs',
                sourcemap: true,
                external: ['path','fs','twig'],
                globals: {
                    twig: 'Twig',
                    marked: 'marked',
                    yaml: 'jsyaml',
                }
            },

            {
                file: 'js/build/grav.field.es.js',
                external: ['path','fs','twig'],
                format: 'esm',
                sourcemap: true
            },
        ]
    },

    {
        input: 'js/forms/grav.fields.js',
        plugins: plugins(true),
        output: [
            {
                file: 'js/build/grav.field.min.js',
                format: 'umd',
                name: 'gravjs',
                sourcemap: true,
                external: ['path','fs','twig'],
                globals: {
                    twig: 'Twig',
                    marked: 'marked',
                    yaml: 'jsyaml',
                }
            },
        ]
    }
];
