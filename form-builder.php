<?php

namespace Grav\Plugin;

use Grav\Common\Data\Blueprints;
use Grav\Common\Plugin;
use RocketTheme\Toolbox\Event\Event;

/**
 * Class FormBuilderPlugin
 * @package Grav\Plugin
 */
class FormBuilderPlugin extends Plugin
{

    public $features = [
        'blueprints' => 1000,
    ];

    /**
     * A form-builder field stores formatted YAML.
     */
    public function getFormFieldTypes()
    {
        return [
            'form-builder' => [
                'yaml' => true,
                'validate' => [
                    'type' => 'yaml'
                ]
            ]
        ];
    }

    /**
     * @return array
     *
     * The getSubscribedEvents() gives the core a list of events
     *     that the plugin wants to listen to. The key of each
     *     array section is the event that the plugin listens to
     *     and the value (in the form of an array) contains the
     *     callable (or function) as well as the priority. The
     *     higher the number the higher the priority.
     */
    public static function getSubscribedEvents()
    {
        return [
            'onPluginsInitialized' => ['onPluginsInitialized', 0],
            'onTwigTemplatePaths' => ['onTwigTemplatePaths', 0]
        ];
    }

    /**
     * Initialize the plugin
     */
    public function onPluginsInitialized()
    {
        // Don't proceed if we are in the admin plugin
        if ($this->isAdmin()) {
            $this->enable([
                'onGetPageTemplates' => ['onGetPageTemplates', 0],
            ]);
            return;
        }

        // Enable the main event we are interested in
        $this->enable([
            'onFormPageHeaderProcessed' => ['onFormPageHeaderProcessed', 0],
        ]);
    }

    public function onTwigTemplatePaths()
    {
        $this->grav['twig']->twig_paths[] = __DIR__ . '/templates';
    }

    public function onGetPageTemplates(Event $event)
    {
        /* @var Page\Types $types */
        $types = $event->types;
        $locator = $this->grav['locator'];
        // Set blueprints & templates.
        $types->scanBlueprints($locator->findResource('plugin://' . $this->name . '/blueprints'));
        // https://github.com/ellioseven/grav-plugin-view/blob/master/view.php#L69
    }

    public function onFormPageHeaderProcessed(Event $event)
    {
        if ($event['header']->form['flag'] ?? false === 'form-builder') {
            $form = $this->makeFormFromBuilder($event['header']->form);
            // Replace page header.form (a configuration blueprint) by a real (built) form:
            $event['header']->form = $form;
        }
    }

    private function fieldSelectionToArray(array $selection)
    {
        $output = [];
        foreach ($selection as $element) {
            $output[$element['y']][$element['x']] = $element;
        }

        return $output;
    }

    private function getFieldBlueprint($field)
    {
        static $blueprints;
        if (!$blueprints) {
            $blueprints = new Blueprints();
        }

        $name = 'form-builder/frontend-field-' . strtolower($field);
        return $blueprints->get($name)->fields() ?? null;
    }

    private function arrayAccessor($array, $address, $delimiter = '.')
    {
        $address = explode($delimiter, $address);
        $numArgs = count($address);

        $val = $array;
        for ($i = 0; $i < $numArgs; $i++) {
            // every iteration brings us closer to the truth
            $val = $val[$address[$i]] ?? null;
        }
        return $val;
    }

    private function dynamicFieldReplace($array, $context)
    {
        if (!is_array($array)) {
            return $array;
        }

        $helper = [];
        foreach ($array as $key => $value) {
            $m = [];
            if (preg_match('/^field-(.+)@$/', $key, $m)) {
                if (is_string($value)) {
                    $helper[$m[1]] = $this->arrayAccessor($context, $value);
                } else {
                    $helper[$m[1]] = array_map(function ($e) use ($context) {
                        return $this->arrayAccessor($context, $e) ?: $e;
                    }, $value);
                }
            } elseif (is_array($value)) {
                $helper[$key] = $this->dynamicFieldReplace($value, $context);
            } else {
                $helper[$key] = $value;
            }
        }

        return $helper;
    }


    private function makeFormFromBuilder($config)
    {
        $selection = $this->fieldSelectionToArray($config['field_selection'] ?? []);
        // ToDo: not implemented yet. See https://github.com/getgrav/grav-plugin-form/issues/388
        $requirements = []; // $config['_required'] ?? [];

        $form = ['fields'             => [],
                 'inline_errors'      => true,
                 'refresh_prevention' => true,
                 'scope'              => ''];

        foreach ($config as $c => $v) {
            if ($c === 'action' || $c === 'id' || $c === 'attributes' || $c === 'classes' || strpos($c, 'data-') === 0) {
                $form[$c] = $v;
            }
        }

        foreach ($selection as $line) {
            if (count($line) > 1) {
                $slug = '';
                $fields = [];
                foreach ($line as $e) {
                    $bl = $this->getFieldBlueprint($e['key']);
                    if ($bl) {
                        if ($slug) {
                            $slug .= '-';
                        }
                        $slug .= $e['key'];
                        if (array_key_exists($e['key'], $requirements)) {
                            $first_key = array_key_first($bl);
                            $bl[$first_key]['validate']['required'] = (bool)($requirements[$e['key']]);
                        }

                        $fields += $bl;
                    }
                }

                $form['fields'] += [
                    $slug => [
                        'type' => 'fieldset',
                        'classes' => 'form-inline',
                        'id'   => $slug,
                        'fields' => $fields
                    ]
                ];
            } else {
                $item = array_pop($line);
                $bl = $this->getFieldBlueprint($item['key']);
                if ($bl) {
                    if (array_key_exists($item['key'], $requirements)) {
                        $first_key = array_key_first($bl);
                        $bl[$first_key]['validate']['required'] = (bool)($requirements[$item['key']]);
                    }
                    $form['fields'] += $bl;
                } else {
                    // ToDo: No blueprint for this.
                    // var_dump($e);die;
                }
            }
        }

        $replaced = $this->dynamicFieldReplace(
            $form['fields'],
            ['form' => $form, 'header' => ['form' => $config]]
        );
        $form['fields'] = $replaced;
        $form['fields'] += ($config['additional_fields'] ?? []) + ($config['fields'] ?? []);

        $defaultButton = ['type' => 'submit', 'value' => $config['button_label'], 'class' => 'btn btn-lg btn-block'];
        if (!isset($form['buttons'])) {
            $form['buttons'] = [];
        }
        if (!isset($form['buttons']['submit'])) {
            $form['buttons']['submit'] = $defaultButton;
        } else {
            $form['buttons']['submit'] += $defaultButton;
        }

        return $form;
    }
}
