# Form Builder Plugin

**This README.md file should be modified to describe the features, installation, configuration, and general usage of the plugin.**

The **Form Builder** Plugin is an extension for [Grav CMS](http://github.com/getgrav/grav). A basic form builder

## Installation

Installing the Form Builder plugin can be done in one of three ways: The GPM (Grav Package Manager) installation method lets you quickly install the plugin with a simple terminal command, the manual method lets you do so via a zip file, and the admin method lets you do so via the Admin Plugin.

### GPM Installation (Preferred)

To install the plugin via the [GPM](http://learn.getgrav.org/advanced/grav-gpm), through your system's terminal (also called the command line), navigate to the root of your Grav-installation, and enter:

    bin/gpm install form-builder

This will install the Form Builder plugin into your `/user/plugins`-directory within Grav. Its files can be found under `/your/site/grav/user/plugins/form-builder`.

### Manual Installation

To install the plugin manually, download the zip-version of this repository and unzip it under `/your/site/grav/user/plugins`. Then rename the folder to `form-builder`. You can find these files on [GitHub](https://github.com/drzraf/grav-plugin-form-builder) or via [GetGrav.org](http://getgrav.org/downloads/plugins#extras).

You should now have all the plugin files under

    /your/site/grav/user/plugins/form-builder
	
> NOTE: This plugin is a modular component for Grav which may require other plugins to operate, please see its [blueprints.yaml-file on GitHub](https://github.com/drzraf/grav-plugin-form-builder/blob/master/blueprints.yaml).

### Admin Plugin

If you use the Admin Plugin, you can install the plugin directly by browsing the `Plugins`-menu and clicking on the `Add` button.

## Configuration

Before configuring this plugin, you should copy the `user/plugins/form-builder/form-builder.yaml` to `user/config/plugins/form-builder.yaml` and only edit that copy.

Here is the default configuration and an explanation of available options:

```yaml
enabled: true
```

Note that if you use the Admin Plugin, a file with your configuration named form-builder.yaml will be saved in the `user/config/plugins/`-folder once the configuration is saved in the Admin.

## Usage

This is a blueprint example:

```yaml
# Sample <theme|plugin>/blueprints/partials/newsletter-form-builder.yaml
form:
  fields:
    # Mandatory, this is the form-builder is processed onFormPageHeaderProcessed and we
    # do not have access to form's blueprint and can't detect which fields are form-builder and process them.
    # This tags shows this forms contains one of them which *must* be named "field_selection".
    header.form.flag:
      type: hidden
      value: form-builder

    # Note: name is hardcoded.
    # See https://github.com/getgrav/grav-plugin-admin/issues/1793
    header.form.field_selection:
      type: form-builder
      # twig: yes is automatic
      label: Form fields layout
      fields:
        # Keys must match an existing blueprint named "partials/form-builder-field-{{ field_name | tolower }}".
        # It's advised that these blueprints use their HTML id as their suffix.
        firstName:
          # Values are always objects, considered as "display" field by default.
          # "content" is then the only mandatory key.
          content: First name
        # but fields can also include subfields fields. In that case they are considered as "section" fields
        country:
          text: Country
          fields:
            # subfields must be fully prefixed
            header.form.default_country:
              label: Default
              # Wordaround for https://github.com/getgrav/grav-plugin-form/issues/387
              type: non-selectize
              config-default@: plugins.petition.default_country
              data-options@: '\Grav\Plugin\PetitionPlugin::countryCodes'
        # If a type is given "button" field does not need to match a blueprint.
        button:
            # Note: name is mandatory!
            name: header.form.button_label
            type: text
            label: An extra button
            default: click-here
      flavors:
        "Default newsletter":
          - key: firstname
            x: 0
            'y': 0
            width: 1
          - key: country
            x: 0
            'y': 1
            width: 2
          - key: cp
            x: 0
            'y': 2
            width: 1
```

Assuming the following blueprints exist:

```yaml
# <theme|plugin>/blueprints/partials/form-builder-field-firstname.yaml
form:
  fields:
    firstName:
      label: First Name
      type: text
      id: firstNameID
      classes: light
```

This is the resulting UI:  

![ui](./screenshot.png)


This is a sample form's page:

```yaml
---
form:
    flag: form-builder
    button_label: Bar
    field_selection:
        -
            key: lastName
            x: 1
            'y': 0
            width: 1
            height: 1
        -
            key: country
            x: 0
            'y': 1
            width: 2
            height: 1
        -
            key: cp
            x: 0
            'y': 2
            width: 1
            height: 1
        -
            key: button
            x: 0
            'y': 3
            width: 2
            height: 1
            value: Bar
```

An additional `header.form.additional_fields` is supported. If set, the corresponding YAML will be appended to the form.  
Example:

```yaml
form:
  fields:
    tabs:
      fields:
        content:
          # The above mentioned partial blueprint.
          import@:
            type: partials/newsletter-form-builder
            context: 'blueprints://'

          # A custom form data-* attributes
          fields:
            header.form.data-newsletter-form:
              type: hidden
              value: true

          # Editor for additional custom fields
          fields:
            header.form.additional_fields:
              type: editor
              label: Additional fields
              validate:
                type: yaml
              codemirror:
                mode: 'yaml'
              default: |
                honey:
                  type: honeypot

```

## To Do

- [ ] Avoid hardcoding `field_selection`.
- [ ] Avoid hardcoding `additional_fields`.
- [ ] Remove assumptions about form's buttons.

## Upstream issues:
- We need Grav to handle nicely admin forms submitted, containing hidden/dynamically generated fields.
  Because we create subfield dynamically and don't want to put all elements at the root of the form but
  rather match the existing blueprint structure.
  - https://github.com/getgrav/grav-plugin-form/issues/388
  - (Not directly blocking, but we also have this long-pending request for custom form attributes which would probably help too: https://github.com/getgrav/grav-plugin-form/pull/336)

- On the UI/GridStack.js side, we would like the widget for new fields to match the "type" of fields. Eg: a Select for a <select>, ...
  But GridStack.js does not allow us to do this: https://github.com/gridstack/gridstack.js/issues/1091

- On a tighter integration we would like to use Grav infrastructure to render fields instead of coding each field by hand.
  This imply rendering Grav's Twig in Javascript and also loading Grav filters in Javascript.
  Most of this has already been done:
  On the PHP-to-ES7 conversion a lot of work has been done on babel-preset-php, with a couple of merge-requests still pending:
  - https://gitlab.com/kornelski/babel-preset-php/-/merge_requests/9 (but not blockingà

  The problem rather lays into Twig non-compliance from Twig javascript parsers. We tried the main two:
  - For Twing: https://github.com/NightlyCommit/twing/issues/388 (loading Twig template in AJAX)
  - For Twig.js: https://github.com/twigjs/twig.js/issues/277 (String interpolation)
  Also good to have (for Twig.js) would be:
  - https://github.com/twigjs/twig.js/issues/696
  - https://github.com/twigjs/twig.js/issues/697
  - https://github.com/twigjs/twig.js/issues/704 (to double-check)
